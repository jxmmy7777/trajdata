from collections import defaultdict

from torch.utils.data import DataLoader
from tqdm import tqdm

from trajdata import AgentBatch, AgentType, UnifiedDataset,SceneBatch
from trajdata.augmentation import NoiseHistories
from trajdata.visualization.vis import plot_agent_batch,plot_scene_batch

# from trajdata.custom_func import get_actions_inversdyn, is_stationary

from trajdata.custom_func.get_waymo_map import get_waymo_map

import matplotlib.pyplot as plt


from trajdata import AgentBatch, AgentType, UnifiedDataset
from trajdata.data_structures.scene_metadata import Scene
from trajdata.data_structures.state import StateArray
from trajdata.simulation import SimulationScene, sim_metrics, sim_stats, sim_vis
from trajdata.visualization.vis import plot_agent_batch


INTERACTIONS_DATASETS_PATH = "/home/msc_lab/ctang/weijer/292B"

def main():
    noise_hists = NoiseHistories()

    dataset = UnifiedDataset(
        # desired_data=["lyft_val"],
        desired_data=["waymo_val"],
        # desired_data=["interaction_single"],
        centric="scene",
        desired_dt=0.1,
        history_sec=(1.0, 1.0),
        future_sec=(8.0, 8.0),
        # only_predict=[AgentType.VEHICLE],
        agent_interaction_distances=defaultdict(lambda: 30.0),
        incl_robot_future=False,
        incl_raster_map=True,
        raster_map_params={
            "px_per_m": 2,
            "map_size_px": 224,
            "offset_frac_xy": (-0.5, 0.0),
        },
        num_workers=0,
        state_format= "x,y,z,xd,yd,xdd,ydd,h",
        obs_format="x,y,z,xd,yd,xdd,ydd,s,c",
        verbose=True,
        data_dirs={  # Remember to change this to match your filesystem!
            # "waymo_val": "/home/msc_lab/datasets/waymo_open_dataset_motion_v_1_1_0/debug",
            "waymo_train": "/mnt/hdd2/waymo",
            "waymo_val":"/mnt/hdd2/waymo/debug",
            "waymo_test":"/mnt/hdd2/waymo",
            # "lyft_val": "~/datasets/lyft-prediction-dataset",
            # "interaction_single":INTERACTIONS_DATASETS_PATH
        },
        extras = {
            # "target_actions": get_actions_inversdyn,
            # "is_stationary": is_stationary,
            "waymo_map":get_waymo_map
        },
        # cache_location= "/mnt/hdd2/weijer/.unified_data_cache/",
        cache_location= "~/.unified_data_cache/",

        save_index = False,
        rebuild_cache=False,
        # filter_fn = stationary_filter
    )


    # dataset.apply_filter(filter_fn=stationary_filter, num_workers=4)
    print(f"# Data Samples: {len(dataset):,}")

    dataloader = DataLoader(
        dataset,
        batch_size=2,
        shuffle=False,
        collate_fn=dataset.get_collate_fn(return_dict=False),
        num_workers=16,
    )

    # batch: AgentBatch
    batch: SceneBatch
    i=0
    for batch in tqdm(dataloader):
        i+=1
        fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        ax = plot_scene_batch(batch,ax=ax, batch_idx=0, show=False, close=False)
        lane_points = batch.extras["map_polylines"][0, ..., :2]
        lane_mask = batch.extras["map_polylines_mask"][0]

        # Plot each polyline that has valid points
        for j in range(768):  # Assuming there are 768 polylines per batch
            if any(lane_mask[j]):  # Only process lanes with valid points
                valid_points = lane_points[j][lane_mask[j]]
                if valid_points.shape[0] > 1:  # Ensure there are enough points to plot
                    ax.plot(
                        valid_points[:, 0],  # X coordinates
                        valid_points[:, 1],  # Y coordinates
                        "o-",  # Line style with markers
                        markersize=3,
                        label=f"Lane {j}" if j == 0 else "_nolegend_"
                    )

        # Customize and save the plot
        ax.set_title(f"Scene {i+1}")
        ax.grid(True)
        ax.legend()
        plt.savefig(f"scene_{i+1}.png")
        plt.close(fig)  # Close the figure to free memory
        i += 1
        
    
        
    
def stationary_filter(elem) -> bool:
    return elem.agent_meta_dict["is_stationary"]


if __name__ == "__main__":
    main()
