"""
This is an example of how to extend a batch with lane information
"""

import random
from collections import defaultdict

import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from trajdata import AgentBatch, AgentType, UnifiedDataset, SceneBatch
from trajdata.data_structures.batch_element import AgentBatchElement
from trajdata.maps import VectorMap
from trajdata.utils.arr_utils import transform_angles_np, transform_coords_np
from trajdata.utils.state_utils import transform_state_np_2d
from trajdata.visualization.vis import plot_agent_batch

from trajdata.maps.vec_map_elements import RoadArea

def get_closest_lane_point(element: AgentBatchElement) -> np.ndarray:
    """Closest lane for predicted agent."""

    # Transform from agent coordinate frame to world coordinate frame.
    vector_map: VectorMap = element.vec_map
    world_from_agent_tf = np.linalg.inv(element.agent_from_world_tf)
    agent_future_xyzh_world = transform_state_np_2d(
        element.agent_future_np, world_from_agent_tf
    ).as_format("x,y,z,h")

    # Use cached kdtree to find closest lane point
    lane_points = []
    for point_xyzh in agent_future_xyzh_world:
        possible_lanes = vector_map.get_current_lane(point_xyzh)
        xyzh_on_lane = np.full((1, 4), np.nan)
        if len(possible_lanes) > 0:
            xyzh_on_lane = possible_lanes[0].center.project_onto(point_xyzh[None, :3])
            # xyzh_on_lane[:100] = possible_lanes[0].center.xyzh[:100]
            xyzh_on_lane[:, :2] = transform_coords_np(
                xyzh_on_lane[:, :2], element.agent_from_world_tf
            )
            xyzh_on_lane[:, -1] = transform_angles_np(
                xyzh_on_lane[:, -1], element.agent_from_world_tf
            )

        lane_points.append(xyzh_on_lane)

    lane_points = np.concatenate(lane_points, axis=0)
    return lane_points

def get_nearby_lane_point(element: AgentBatchElement) -> np.ndarray:
    """Closest lane for predicted agent."""

    # Transform from agent coordinate frame to world coordinate frame.
    vector_map: VectorMap = element.vec_map
    world_from_agent_tf = np.linalg.inv(element.agent_from_world_tf)
    agent_future_xyzh_world = transform_state_np_2d(
        element.agent_future_np, world_from_agent_tf
    ).as_format("x,y,z,h")

    # Use cached kdtree to find closest lane point
    lane_points = []
    for point_xyzh in agent_future_xyzh_world:
        possible_lanes = vector_map.get_lanes_within(point_xyzh[:3], dist = 50.0)
        xyzh_on_lane = np.full((100, 4), np.nan)
        if len(possible_lanes) > 0:
            xyzh_on_lane = possible_lanes[0].center.project_onto(point_xyzh[None, :3])
            xyzh_on_lane = possible_lanes[0].center.xyzh[:100]
            xyzh_on_lane[:, :2] = transform_coords_np(
                xyzh_on_lane[:, :2], element.agent_from_world_tf
            )
            xyzh_on_lane[:, -1] = transform_angles_np(
                xyzh_on_lane[:, -1], element.agent_from_world_tf
            )

        lane_points.append(xyzh_on_lane)

    lane_points = np.concatenate(lane_points, axis=0)
    return lane_points

def main():
    dataset = UnifiedDataset(
        # desired_data=["lyft_val"],
        desired_data=["waymo_val"],
        # desired_data=["interaction_single"],
        centric="agent",
        desired_dt=0.1,
        history_sec=(1.0, 1.0),
        future_sec=(3.0, 3.0),
        only_predict=[AgentType.VEHICLE],
        agent_interaction_distances=defaultdict(lambda: 30.0),
        incl_robot_future=False,
        incl_vector_map=True,
        incl_raster_map=True,
        raster_map_params={
            "px_per_m": 2,
            "map_size_px": 224,
            "offset_frac_xy": (-0.5, 0.0),
        },
        num_workers=0,
        obs_format="x,y,z,xd,yd,xdd,ydd,s,c",
        verbose=True,
        data_dirs={  # Remember to change this to match your filesystem!
            # "waymo_val": "/home/msc_lab/datasets/waymo_open_dataset_motion_v_1_1_0/debug",
            # "waymo_train": "/mnt/hdd2/waymo",
            "waymo_val":"/mnt/hdd2/waymo",
            # "lyft_val": "~/datasets/lyft-prediction-dataset",
            # "interaction_single":INTERACTIONS_DATASETS_PATH
        },
        vector_map_params = {
            "incl_road_lanes": True,
            "incl_road_areas": True,
            "incl_ped_crosswalks": True,
            "incl_ped_walkways": True,
            "collate" : True
        },
        
        cache_location= "/mnt/hdd2/weijer/.unified_data_cache/",
        save_index = False,
        # filter_fn = stationary_filter
  
        # A dictionary that contains functions that generate our custom data.
        # Can be any function and has access to the batch element.
        extras={
            "closest_lane_point": get_closest_lane_point,
            # "closest_lane_point": get_nearby_lane_point,
        },
    )

    print(f"# Data Samples: {len(dataset):,}")

    dataloader = DataLoader(
        dataset,
        batch_size=4,
        shuffle=False,
        collate_fn=dataset.get_collate_fn(),
        num_workers=0,
    )

    # Visualize selected examples
    num_plots = 30
    # batch_idxs = [10876, 10227, 1284]
    batch_idxs = random.sample(range(len(dataset)), num_plots)
    batch: AgentBatch = dataset.get_collate_fn(pad_format="right")(
        [dataset[i] for i in batch_idxs]
    )
    assert "closest_lane_point" in batch.extras

    for batch_i in range(num_plots):
        ax = plot_agent_batch(
            batch, batch_idx=batch_i, legend=False, show=False, close=False
        )
        # fig, ax = plt.subplots(1, 1, figsize=(10, 10))
        #visualize all lane points
        vec_map = batch.vector_maps[batch_i]
        min_x, min_y, _, max_x, max_y, _ = vec_map.extent

        mean_pt: np.ndarray = np.array(
            [
                np.random.uniform(min_x, max_x),
                np.random.uniform(min_y, max_y),
                0,
            ]
        )
        map_img, raster_from_world = vec_map.rasterize(
            resolution=2,
            return_tf_mat=True,
            incl_centerlines=True,
            area_color=(255, 255, 255),
            edge_color=(0, 0, 0),
            scene_ts=100,
        )
        
        # batch.agent_from_world_tf = raster_from_world
        agent_from_world =  batch.agents_from_world_tf[batch_i].numpy()
        raster_from_world = batch.rasters_from_world_tf[batch_i].numpy()
        world_from_raster = np.linalg.inv(raster_from_world)
        agent_from_raster= agent_from_world @ world_from_raster
        lanes = vec_map.get_lanes_within(mean_pt, 200)
        for l in lanes:
            # vec_map.visualize_lane_graph(
            #     origin_lane=l,
            #     num_hops=0,
            #     raster_from_world=agent_from_world,
            #     ax=ax,
            #     legend=False,
            # )
            lane_pts = l.center.xyzh[...,:2]
            lane_pts = transform_coords_np(
                lane_pts[:, :2], agent_from_world
            )
            ax.plot(
                lane_pts[:, 0],
                lane_pts[:, 1],
                "o-",
                markersize=1,
                color="red",
                # label="Nearby points",
            )
            
        lane_points = batch.extras["closest_lane_point"][batch_i]
        lane_points = lane_points[
            torch.logical_not(torch.any(torch.isnan(lane_points), dim=1)), :
        ].numpy()

        ax.plot(
            lane_points[:, 0],
            lane_points[:, 1],
            "o-",
            markersize=3,
            label="Lane points",
        )

        ax.legend(loc="best", frameon=True)

        plt.savefig(f"{batch_i}")
        plt.close()

    # Scan through dataset
    batch: AgentBatch
    for idx, batch in enumerate(tqdm(dataloader)):
        assert "closest_lane_point" in batch.extras
        if idx > 50:
            break


if __name__ == "__main__":
    main()
