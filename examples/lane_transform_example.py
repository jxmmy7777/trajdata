"""
This is an example of how to extend a batch with lane information
"""

import random
from collections import defaultdict

from typing import Any, Dict, Iterable, List, Mapping, Optional, Sequence, Tuple, Union, Callable
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm

from trajdata import AgentBatch, AgentType, UnifiedDataset
from trajdata.data_structures.batch_element import AgentBatchElement
from trajdata.maps import VectorMap
from trajdata.maps.vec_map_elements import RoadLane
from trajdata.maps.ref_utils import ref_path
from trajdata.maps.centerline_utils import *

from trajdata.utils.arr_utils import transform_angles_np, transform_coords_np
from trajdata.utils.state_utils import transform_state_np_2d
from trajdata.visualization.vis import plot_agent_batch

from trajdata.custom_func.get_lane_info import get_lane_info

def transform_FRT2Cart_poly_torch_batch(nt_arr: torch.Tensor, poly_coeffs: torch.Tensor, t_boundaries: torch.Tensor) -> torch.Tensor:
    """Transform 2D nt array from Frenet Serret coordinates to Cartesian space using piece-wise polynomials."""
    t_arr = nt_arr[:, :, 1:]
    # Modified: Clamp t_arr right at the beginning
    t_max = t_boundaries[:, -1]
    zero_tensor  = torch.zeros_like(t_max)
    t_arr = torch.clamp(t_arr, zero_tensor.unsqueeze(-1).unsqueeze(-1), t_max.unsqueeze(-1).unsqueeze(-1))
    # Assuming t_arr and n_arr have the same batch and sequence dimensions
    clamped_indices = (t_arr <= 0) | (t_arr >= t_max.unsqueeze(-1).unsqueeze(-1))

    # Set the corresponding n_arr values to zero
    n_arr = nt_arr[:, :, 0:1]
    n_arr[clamped_indices] = 0.0

    xy_arr = torch.zeros_like(nt_arr)

    for i in range(t_boundaries.size(1)):
        if i == 0:
            segment_mask = nt_arr[:, :, 1] <= t_boundaries[:, i].unsqueeze(1)
        elif i == t_boundaries.size(1) - 1:
            segment_mask = nt_arr[:, :, 1] > t_boundaries[:, i - 1].unsqueeze(1)
        else:
            segment_mask = (nt_arr[:, :, 1] > t_boundaries[:, i - 1].unsqueeze(1)) & (nt_arr[:, :, 1] <= t_boundaries[:, i].unsqueeze(1))

        # subtract t_boundary from t for segments other than the first
        if i > 0:
            segment_t_arr = t_arr - t_boundaries[:, i - 1].unsqueeze(-1).unsqueeze(-1)
            assert (segment_t_arr[segment_mask]>=0).all() # make sure that after subtracting the t_boundary, the value is still >= 0
        else:
            segment_t_arr = t_arr

        segment_t_sqr_arr = torch.cat([segment_t_arr ** 4, segment_t_arr ** 3, segment_t_arr ** 2, segment_t_arr, torch.ones_like(segment_t_arr)], dim=-1)
        assert torch.isfinite(segment_t_sqr_arr).all(), segment_t_sqr_arr

        segment_poly_coeffs = poly_coeffs[:, i]

        # Apply mask to set values outside segment_mask to zero
        segment_t_sqr_arr[~segment_mask.unsqueeze(-1).expand_as(segment_t_sqr_arr)] = 0.0

        if segment_t_sqr_arr.sum() == 0:
            continue
        # Calculate the polynomial coefficients for x and y
        coeff_x, coeff_y = segment_poly_coeffs[:, 0], segment_poly_coeffs[:, 1]

        # Calculate the gradient of the polynomial coefficients
        grad_coeff_x = (coeff_x * torch.tensor([4.0, 3.0, 2.0, 1.0, 0.0], device=coeff_x.device).unsqueeze(0))[:, :-1]
        grad_coeff_y = (coeff_y * torch.tensor([4.0, 3.0, 2.0, 1.0, 0.0], device=coeff_y.device).unsqueeze(0))[:, :-1]

        # Calculate the tangent vector coefficients
        t_coeff = torch.stack((grad_coeff_x, grad_coeff_y), dim=1)

        # Calculate the normal vector coefficients
        normal_vec = torch.tensor([[0.0, -1.0], [1.0, 0.0]], device=t_coeff.device).repeat(t_coeff.shape[0],1,1)
        d_coeff = torch.bmm(normal_vec, t_coeff).transpose(1,2)

        # Offset the xy_arr by the normal vector coefficients
        d_arr = torch.bmm(segment_t_sqr_arr[..., 1:], d_coeff)

        # Normalize the d_arr if it is not zero
        d_arr_norm = torch.norm(d_arr, dim=-1, keepdim=True)
        d_arr_norm = torch.where(d_arr_norm == 0, torch.ones_like(d_arr_norm), d_arr_norm)

        d_arr = d_arr/ d_arr_norm

        xy_segment_arr = torch.bmm(segment_t_sqr_arr, segment_poly_coeffs.transpose(-1, -2))
        xy_segment_arr = xy_segment_arr + n_arr * d_arr


        xy_arr[segment_mask.unsqueeze(-1).expand_as(xy_arr)]= xy_segment_arr[segment_mask.unsqueeze(-1).expand_as(xy_segment_arr)] #TODO check whether this is valid
        # xy_arr += xy_segment_arr
    return xy_arr
def main():
    dataset = UnifiedDataset(
        # desired_data=["lyft_val"],
        desired_data=["waymo_train"],
        centric="agent",
        desired_dt=0.1,
        history_sec=(3.2, 3.2),
        future_sec=(4.8, 4.8),
        only_predict=[AgentType.VEHICLE],
        agent_interaction_distances=defaultdict(lambda: 30.0),
        incl_robot_future=False,
        incl_raster_map=True,
        raster_map_params={
            "px_per_m": 2,
            "map_size_px": 224,
            "offset_frac_xy": (-0.5, 0.0),
        },
        num_workers=0,
        incl_vector_map=True,
        only_types=[AgentType.VEHICLE],
        obs_format="x,y,z,xd,yd,xdd,ydd,s,c",
        verbose=True,
        data_dirs={  # Remember to change this to match your filesystem!
            "waymo_train": "~/datasets/waymo_open_dataset_motion",
            # "lyft_train": "~/datasets/lyft-prediction-dataset/scenes/train.zarr",
            # "lyft_val": "~/datasets/lyft-prediction-dataset"
        },
        # A dictionary that contains functions that generate our custom data.
        # Can be any function and has access to the batch element.
        extras={
        "closest_lane_point": get_lane_info,
        }
    )

    print(f"# Data Samples: {len(dataset):,}")

    dataloader = DataLoader(
        dataset,
        batch_size=32,
        shuffle=False,
        collate_fn=dataset.get_collate_fn(),
        num_workers=0,
    )

    # Visualize selected examples
    num_plots = 3
    # batch_idxs = [10876, 10227, 1284]
    batch_idxs = random.sample(range(len(dataset)), num_plots)
    batch: AgentBatch = dataset.get_collate_fn(pad_format="right")(
        [dataset[i] for i in batch_idxs]
    )

    #transformation

    tested_path = torch.zeros_like(batch.extras["nt_delta_target"])
    tested_path[:,:,1] = 1.0*5
    tested_path[:,:,0] = 0.1
    nt_arr = torch.cumsum(tested_path,dim=1)
    xy_traj_batch = transform_FRT2Cart_poly_torch_batch(nt_arr,
                                    batch.extras["polynomial_coeff"]
                                    ,batch.extras["t_boundaries"])
    
    gt_nt_arr = batch.extras["nt_delta_target"]
    gt_nt_arr = torch.cumsum(gt_nt_arr, dim=1)+batch.extras["nt_current"].unsqueeze(1)
    gt_xy_traj_batch = transform_FRT2Cart_poly_torch_batch(gt_nt_arr,
                                    batch.extras["polynomial_coeff"]
                                    ,batch.extras["t_boundaries"])

    for batch_i in range(num_plots):
        if not batch.extras["has_lane"][batch_i]:
            continue
        ax = plot_agent_batch(
            batch, batch_idx=batch_i, legend=False, show=False, close=False
        )
        lane_points = batch.extras["all_poss_refs"][batch_i].reshape(-1,2)
        lane_points = lane_points[
            torch.logical_not(torch.any(torch.isnan(lane_points), dim=1)), :
        ].numpy()
        
        # ax.plot(
        #     lane_points[:, 0],
        #     lane_points[:, 1],
        #     "o-",
        #     markersize=3,
        #     label="All possible Lane points",
        # )
        ax.scatter(
            lane_points[:, 0],
            lane_points[:, 1],
            # "o-",
            # markersize=3,
            s = 1,
            color = "grey",
            label="All possible Lane points",
            
        )

        closest_lane_points = batch.extras["centerline_xy"][batch_i]
        closest_lane_points = closest_lane_points[
            torch.logical_not(torch.any(torch.isnan(closest_lane_points), dim=1)), :
        ].numpy()

        ax.plot(
            closest_lane_points[:, 0],
            closest_lane_points[:, 1],
            "o-",
            markersize=1.5,
            color = "orange",
            label="Centerline points",
        )
        # ------------------------left right lane ---------------------------------------
        closest_lane_points = batch.extras["left_centerline_xy"][batch_i]
        closest_lane_points = closest_lane_points[
            torch.logical_not(torch.any(torch.isnan(closest_lane_points), dim=1)), :
        ].numpy()
        if not (closest_lane_points==0).all():
            ax.plot(
                closest_lane_points[:, 0],
                closest_lane_points[:, 1],
                "o-",
                markersize=1.5,
                color = "blue",
                label="left Centerline points",
            )

        closest_lane_points = batch.extras["right_centerline_xy"][batch_i]
        closest_lane_points = closest_lane_points[
            torch.logical_not(torch.any(torch.isnan(closest_lane_points), dim=1)), :
        ].numpy()
        if not (closest_lane_points==0).all():
            ax.plot(
                closest_lane_points[:, 0],
                closest_lane_points[:, 1],
                "o-",
                markersize=1.5,
                color = "red",
                label="right centerline points",
            )

        # -------------- fitted polynomials ---------------"
        # "
        

        # ax.plot(
        #     xy_traj_batch[batch_i,:, 0],
        #     xy_traj_batch[batch_i,:, 1],
        #     "o-",
        #     markersize=1.5,
        #     color = "red",
        #     label="transfomed poly points",
        # )

        # ax.plot(
        #     gt_xy_traj_batch[batch_i,:, 0],
        #     gt_xy_traj_batch[batch_i,:, 1],
        #     "o-",
        #     markersize=1.5,
        #     color = "black",
        #     label="gt sd2xy points",
        # )

        ax.legend(loc="best", frameon=True)
        plt.title(str(torch.linalg.norm(batch.agent_fut[batch_i,...,:2]-gt_xy_traj_batch[batch_i])))
        plt.show()
        plt.savefig(f"visualization/lane_future_lanes{batch_i}")
        plt.close("all")

    # Scan through dataset
    batch: AgentBatch
    for idx, batch in enumerate(tqdm(dataloader)):
        
        if 'scene-0100' in batch.scene_ids:
            for batch_i in range(10):
                ax = plot_agent_batch(
                    batch, batch_idx=batch_i, legend=False, show=False, close=False
                )
                lane_points = batch.extras["all_poss_refs"][batch_i].reshape(-1,2)
                lane_points = lane_points[
                    torch.logical_not(torch.any(torch.isnan(lane_points), dim=1)), :
                ].numpy()
                
                # ax.plot(
                #     lane_points[:, 0],
                #     lane_points[:, 1],
                #     "o-",
                #     markersize=3,
                #     label="All possible Lane points",
                # )
                ax.scatter(
                    lane_points[:, 0],
                    lane_points[:, 1],
                    # "o-",
                    # markersize=3,
                    s = 1,
                    color = "grey",
                    label="All possible Lane points",
                    
                )

                closest_lane_points = batch.extras["centerline_xy"][batch_i]
                closest_lane_points = closest_lane_points[
                    torch.logical_not(torch.any(torch.isnan(closest_lane_points), dim=1)), :
                ].numpy()

                ax.plot(
                    closest_lane_points[:, 0],
                    closest_lane_points[:, 1],
                    "o-",
                    markersize=1.5,
                    color = "orange",
                    label="Centerline points",
                )
                # ------------------------left right lane ---------------------------------------
                closest_lane_points = batch.extras["left_centerline_xy"][batch_i]
                closest_lane_points = closest_lane_points[
                    torch.logical_not(torch.any(torch.isnan(closest_lane_points), dim=1)), :
                ].numpy()
                if not (closest_lane_points==0).all():
                    ax.plot(
                        closest_lane_points[:, 0],
                        closest_lane_points[:, 1],
                        "o-",
                        markersize=1.5,
                        color = "blue",
                        label="left Centerline points",
                    )

                closest_lane_points = batch.extras["right_centerline_xy"][batch_i]
                closest_lane_points = closest_lane_points[
                    torch.logical_not(torch.any(torch.isnan(closest_lane_points), dim=1)), :
                ].numpy()
                if not (closest_lane_points==0).all():
                    ax.plot(
                        closest_lane_points[:, 0],
                        closest_lane_points[:, 1],
                        "o-",
                        markersize=1.5,
                        color = "red",
                        label="right centerline points",
                    )

                # -------------- fitted polynomials ---------------"
                # "
                

                # ax.plot(
                #     xy_traj_batch[batch_i,:, 0],
                #     xy_traj_batch[batch_i,:, 1],
                #     "o-",
                #     markersize=1.5,
                #     color = "red",
                #     label="transfomed poly points",
                # )

                # ax.plot(
                #     gt_xy_traj_batch[batch_i,:, 0],
                #     gt_xy_traj_batch[batch_i,:, 1],
                #     "o-",
                #     markersize=1.5,
                #     color = "black",
                #     label="gt sd2xy points",
                # )

                ax.legend(loc="best", frameon=True)
                plt.title(str(torch.linalg.norm(batch.agent_fut[batch_i,...,:2]-gt_xy_traj_batch[batch_i])))
                plt.show()
                plt.savefig(f"visualization/debug_lane_future_lanes{batch_i}_")
                plt.close("all")

        pass


if __name__ == "__main__":
    main()
