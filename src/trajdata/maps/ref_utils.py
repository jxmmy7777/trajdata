from dataclasses import dataclass, field
from trajdata.maps.vec_map_elements import (
    MapElement,
    MapElementType,
    PedCrosswalk,
    PedWalkway,
    Polyline,
    RoadArea,
    RoadLane,
)
from typing import (
    DefaultDict,
    Dict,
    Iterator,
    List,
    Optional,
    Set,
    Tuple,
    Union,
    overload,
)
import numpy as np
import matplotlib.pyplot as plt
@dataclass
class ref_path:
    ''' reference path of frenet-serret coordinates
        The ref_path consists: 
            1) sampled points 
            2) analytic information 
            3) sd_transformation xy->sd
            4) List of RoadLane IDs that constitues the reference
            5) List of Possible future reference 
        ------------------------
        attributes:
            id: List of RoadLaneids
            ---------sample points--------
            centerline_xy: xy np.narray of the reference path
            s: corresponding s information 
            --------analytic information--------
            ref_s_boundaries: boundaries of each spline path ( determine which spline currently in) 
            x_coeff_arr: coeeff of x(s): x = a0+a1*s+a2*s**2+a3*s**3
            y_coeff_arr: coeeff of y(s): y = b0+b1*s+b2*s**2+b3*s**3
    '''
    road_lane_list: List[RoadLane] 
    polyline_ids: List[str] = field(default_factory=list)
    # path: np.ndarray = np.zeros
    # s: np.ndarray
    # ref_s_boundaries: List[float]  = field(default_factory=list)
    # x_coeff_arr: np.ndarray 
    # y_coeff_arr: np.ndarray
    
    def __post_init__(self) -> None:
        # This function will be called multiple times of the same id
        self.polyline_ids = [lane.id for lane in self.road_lane_list]
        
        centerline_xyzh = []
        centerline_h  = []
        ref_s_boundaries = [0]
        s0 = 0
     
        for lane in self.road_lane_list:
            s0 += lane.center.total_distance
            centerline_xyzh.append(lane.center.xyzh)
            ref_s_boundaries.append(s0)
       

        self.ref_s_boundaries = ref_s_boundaries
        self.centerline_xyzh_list = centerline_xyzh
        centerline_xyzh    = np.vstack(centerline_xyzh)
   
        self.centerline_xy = centerline_xyzh[:,:2]
        self.centerline_h  = centerline_xyzh[:,-1:]
        self.s             = self.calculate_longitudinal_distance(self.centerline_xy)
        self.ref_s_boundaries = ref_s_boundaries
        self.last_lane = lane

       
   
    @staticmethod
    def calculate_longitudinal_distance(xy_coordinates):
        distances = np.linalg.norm(np.diff(xy_coordinates, axis=0), axis=1)
        s = np.insert(np.cumsum(distances), 0, 0.0)
        return s

    @staticmethod
    def fit_polynomials(xy_arr: np.ndarray,s_arr: np.ndarray , deg =4) -> np.ndarray:

        coeff_x = np.polyfit(s_arr, xy_arr[:,0], deg=deg)
        coeff_y = np.polyfit(s_arr, xy_arr[:,1], deg=deg)

        return coeff_x,coeff_y
    
    def get_current_lane(self, xy_point: np.ndarray)->RoadLane:
        """
        Given an XY point, this function finds the current lane in the road_lane_list by:
        1) Calculating its traveled length on the lane
        2) Using s.boundaries to determine the current lane
        
        Args:
            xy_point (np.ndarray): A point in XY coordinates.
        
        Returns:
            current_lane (RoadLane): The current lane containing the xy_point.
        """

        # Calculate the distance along each lane
        from trajdata.maps.centerline_utils import get_nt_distance
        def visualize_xy_point_and_centerline(xy_point: np.ndarray, centerline_xy: np.ndarray):
            """
            Visualizes the given XY point and the centerline.

            Args:
                xy_point (np.ndarray): A point in XY coordinates.
                centerline_xy (np.ndarray): The centerline coordinates.

            Returns:
                None
            """
            # Plot the centerline
            plt.figure()
            plt.plot(centerline_xy[:, 0], centerline_xy[:, 1], label='Centerline', color='blue')

            # Plot the xy_point
            plt.scatter(xy_point[:,0],xy_point[:,1], label='XY Point', color='red')

            plt.legend()
            plt.xlabel('X')
            plt.ylabel('Y')
            plt.title('XY Point and Centerline Visualization')
            plt.grid(True)
            plt.axis('equal')
            plt.savefig("debug_xy_centerline")
            plt.close()
        
        # visualize_xy_point_and_centerline(xy_point[-1:,:],self.centerline_xy)

        nt_arr = get_nt_distance(xy_point[-1:,:],self.centerline_xy)

        # Find the index of the lane containing the xy_point
        current_lane_idx = np.digitize(nt_arr[-1:,1], self.ref_s_boundaries).item() - 1

        if current_lane_idx >= len(self.ref_s_boundaries) - 1:
            current_lane_idx = len(self.ref_s_boundaries) - 2
            print("Warning: current_lane_idx is greater than len(). It has been set to len-1.")

        # Check if current_lane_idx is out of bounds on the low end
        if current_lane_idx < 0:
            current_lane_idx = 0
            print("Warning: current_lane_idx is less than 0. It has been set to 0.")
        # Return the current lane
        current_lane = self.road_lane_list[current_lane_idx]

        return current_lane 
    
    def check_extend_future_lane(self, point) -> bool:
        '''Extend the future reference based on current point (for simulation)
        '''
        # self.plot_centerline_xy(point)
        #check whether need to extend
        last_lane_point = self.last_lane.center.xy[-1]

        return np.linalg.norm(point-last_lane_point)<100
    
    def extend_future_lane(self, next_lane_list):
        '''Extend the future reference based on current point (for simulation)
        '''
        self.polyline_ids.extend([lane.id for lane in next_lane_list])
        self.road_lane_list.extend(next_lane_list)
        s0 = self.ref_s_boundaries[-1]
        for lane  in next_lane_list:
            s0 += lane.center.total_distance
            self.centerline_xyzh_list.append(lane.center.xyzh)
            self.ref_s_boundaries.append(s0)
        
        centerline_xyzh    = np.vstack(self.centerline_xyzh_list)

        self.centerline_xy = centerline_xyzh[:,:2]
        self.centerline_h  = centerline_xyzh[:,-1:]
        self.s             = self.calculate_longitudinal_distance(self.centerline_xy)
        self.last_lane = lane

        pass

        
    def plot_centerline_xy(self, traj=None):
       

        """
        Plot each centerline xy as a different color.

        Args:
        - centerline_xy: Numpy array of shape (num_points, 2) representing the centerline XY coordinates.
        - polyline_ids: List of polyline IDs corresponding to each centerline segment.

        Returns:
        - None (displays the plot).
        """
        plt.figure(figsize=(8, 6))

        for i,polyline_id in enumerate(self.polyline_ids):

            xy = self.centerline_xyzh_list[i][...,:2]

            plt.plot(xy[:, 0], xy[:, 1], label=f'Polyline ID {polyline_id}')
            plt.plot(xy[-1, 0], xy[-1, 1], 'ro')
        # plt.plot(self.centerline_xy[:, 0], self.centerline_xy[:, 1])
        # plt.plot(self.centerline_xy[-1, 0], self.centerline_xy[-1, 1], 'bo')
        
        if traj is not None:
            plt.plot(traj[:, 0], traj[:, 1], label="traj")
            plt.plot(traj[-1, 0], traj[-1, 1], 'bo')
        
        # plt.plot(next_centerline_xy[:, 0], next_centerline_xy[:, 1])
        # plt.plot(next_centerline_xy[-1, 0], next_centerline_xy[-1, 1], 'bo')
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.legend()
        plt.grid(True)
        plt.savefig("centerline_check")
        print("")

    def ShortCubicSpline(self,cur_s=30, fit_len = 80, deg=4):
        ''' ShortCubicSpline takes in a cur_s, and fit a polynomial to from cur_s ->cur_s +fit_len*0.5m
            Because the reference trajecotries are long, we want a function describes the trajectories without piecewise polynomial
            Input: 
                    ref_path
                    cur_s: (1,) current s on the reference path
            Output
                    coeff.x: (deg)
                    coeff.y: (deg)
                    ref_xy: (1,2) reference point of the spline
                    ref_s0: the ref s0 on the long reference
            current polyfit function f_sd_xy(sd)->xy
                x = a0+a1*s+a2*s**2+a3*s**3..
                y = b0+b1*s+b2*s**2+b3*s**3..
            returns 
            
        '''
        ## TODO change the discrete path to analytic path to handle s not on the points
        path =  self.centerline_xy
        indices_arr = np.arange(len(self.ref_s_boundaries))
        indices_arr = np.expand_dims(indices_arr,axis=1)

        startIdx = indices_arr[self.ref_s_boundaries>=cur_s][0]-1 ##-1 for starting idx should <cur_s
        slice_s =  self.ref_s_boundaries[startIdx:startIdx+fit_len]
        slice_path = path[startIdx:startIdx+fit_len,:]

        delta_slice_s = slice_s[:fit_len]-slice_s[0]
        delta_path = slice_path[:fit_len]-slice_path[0]
        coeff_x = np.polyfit(delta_slice_s.squeeze(), delta_path[:,0], deg=deg)
        coeff_y = np.polyfit(delta_slice_s.squeeze(), delta_path[:,1], deg=deg)
        ref_xy = slice_path[0]
        ref_s0 = slice_s[0]

        return coeff_x,coeff_y,ref_xy,ref_s0

