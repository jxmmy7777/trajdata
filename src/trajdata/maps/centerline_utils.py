import datetime
import math
from typing import Iterable, List, Sequence, Set, Tuple

import matplotlib.pyplot as plt
import numpy as np
from shapely.geometry import LinearRing, LineString, Point, Polygon

from scipy.spatial.distance import euclidean
from trajdata.maps.ref_utils import ref_path

def is_overlapping_lane_seq(lane_seq1: Sequence[str], lane_seq2: Sequence[str]) -> bool:
    """
    Check if the 2 lane sequences are overlapping.
    Overlapping is defined as::

        s1------s2-----------------e1--------e2

    Here lane2 starts somewhere on lane 1 and ends after it, OR::

        s1------s2-----------------e2--------e1

    Here lane2 starts somewhere on lane 1 and ends before it

    Args:
        lane_seq1: list of lane ids
        lane_seq2: list of lane ids

    Returns:
        bool, True if the lane sequences overlap
    """

    if lane_seq2[0] in lane_seq1[1:] and lane_seq1[-1] in lane_seq2[:-1]:
        return True
    elif set(lane_seq2) <= set(lane_seq1):
        return True
    return False

def remove_overlapping_lane_seq(lane_seqs: Sequence[Sequence[str]]) -> List[Sequence[str]]:
    """
    Remove lane sequences which are overlapping to some extent

    Args:
        lane_seqs (list of list of integers): List of sequence of lane ids (Eg. [[12345, 12346, 12347], [12345, 12348]])

    Returns:
        List of sequence of lane ids (e.g. ``[[12345, 12346, 12347], [12345, 12348]]``)
    """
    redundant_lane_idx: Set[int] = set()
    for i in range(len(lane_seqs)):
        for j in range(len(lane_seqs)):
            if i in redundant_lane_idx or i == j:
                continue
            if is_overlapping_lane_seq(lane_seqs[i], lane_seqs[j]):
                redundant_lane_idx.add(j)

    unique_lane_seqs = [lane_seqs[i] for i in range(len(lane_seqs)) if i not in redundant_lane_idx]
    return unique_lane_seqs

def filter_candidate_centerlines(
    xy: np.ndarray, ref_path_list: List[ref_path], stationary_threshold: float = 2.0, max_dist_margin: float = 4.0
) -> List[ref_path]:
    """Filter candidate centerlines based on the distance travelled along the centerline.

    Args:
        xy: Trajectory coordinates.
        candidate_cl: List of candidate centerlines.
        stationary_threshold: minimum displacement to be called as non-stationary.
        max_dist_margin:

    Returns:
        filtered_candidate_centerlines: filtered list of candidate centerlines

    """

    # Check if stationary
    if math.sqrt((xy[0, 0] - xy[-1, 0]) ** 2 + (xy[0, 1] - xy[-1, 1]) ** 2) < stationary_threshold:
        stationary = True
    else:
        stationary = False

    # Filtering candidates to retain only those with distance along centerline close to traj length
    # Fit a second order polynomial and find trajectory length
    # if xy.shape[0] > 2:
    #     POLY_ORDER = 1
    #     poly = np.poly1d(np.polyfit(xy[:, 0], xy[:, 1], POLY_ORDER))
    #     obs_y_smooth = [poly(x) for x in xy[:, 0]]
    #     xy_smooth = [(xy[i, 0], obs_y_smooth[i]) for i in range(xy.shape[0])]
    # else:
    #     xy_smooth = xy
    xy_smooth = xy
    traj_len = LineString(xy_smooth).length

    filtered_candidate_centerlines = []
     # If no candidates remain after filtering, incrementally relax the max_dist_margin
    increment_step = 3.0  # The step size to increase max_dist_margin
    max_tries = 2  # The number of times we'll try to increment max_dist_margin

    tries = 0
    while len(filtered_candidate_centerlines) == 0 and tries < max_tries:
        tries += 1
        max_dist_margin += increment_step

        for ref in ref_path_list:
            centerline = ref.centerline_xy
            centerLine = LineString(centerline)
            start_dist = centerLine.project(Point(xy[0, 0], xy[0, 1]))
            end_dist = centerLine.project(Point(xy[-1, 0], xy[-1, 1]))
            dist_along_cl = end_dist - start_dist
            if dist_along_cl > traj_len - max_dist_margin and dist_along_cl < traj_len + max_dist_margin:
                filtered_candidate_centerlines.append(ref)

    assert len(filtered_candidate_centerlines) >0
    return filtered_candidate_centerlines

def get_centerlines_most_aligned_with_trajectory(xy: np.ndarray, ref_path_list: List[ref_path]) -> List[ref_path]:
    """Get the centerline from candidate_cl along which the trajectory travelled maximum distance

    Args:
        xy: Trajectory coordinates
        candidate_cl: List of candidate centerlines

    Returns:
        candidate_centerlines: centerlines along which distance travelled is maximum
    """

    max_dist_along_cl = -float("inf")
    for ref in ref_path_list:
        centerline = ref.centerline_xy
    # for centerline in candidate_cl:
        centerline_linestring = LineString(centerline)
        start_dist = centerline_linestring.project(Point(xy[0, 0], xy[0, 1]))
        end_dist = centerline_linestring.project(Point(xy[-1, 0], xy[-1, 1]))
        dist_along_cl = end_dist - start_dist
        if max_dist_along_cl < -100 or dist_along_cl > max_dist_along_cl + 1:
            max_dist_along_cl = dist_along_cl
            candidate_centerlines = [ref]
        elif dist_along_cl > max_dist_along_cl - 1:
            candidate_centerlines.append(ref)
            max_dist_along_cl = max(max_dist_along_cl, dist_along_cl)

    return candidate_centerlines

def truncate_ref_path(test_path, ref_path):
    '''
        Need to find the nearest start and end point of the selected reference path
    '''
    startPt = test_path[0,:]
    endPt = test_path[-1,:]
    ref_path_startIdx = np.argmin(np.sum((startPt - ref_path)**2, axis=1))
    #print 'ref_path_startIdx = ', ref_path_startIdx

    ref_path_endIdx = np.argmin(np.sum((endPt - ref_path)**2, axis=1))
    path_length = test_path.shape[0]

    # This is a special case, and there might be a better way to handle it
    if ref_path_startIdx >= ref_path_endIdx:
        ref_path_startIdx = 0
        ref_path_endIdx = ref_path_startIdx + path_length
        # holder = ref_path_endIdx
        # ref_path_endIdx = ref_path_startIdx
        # ref_path_startIdx = holder

    # First find the closest start point on reference path
    # Then choose the appropriate range for the truncated reference path

    if (ref_path_startIdx + 1 + path_length) > ref_path.shape[0]:
        ref_path_truncated = ref_path[ref_path_startIdx:-1, :]
        ref_path_endIdx = -1
    else:
        if (ref_path_startIdx + path_length) < ref_path_endIdx:
            ref_path_truncated = ref_path[ref_path_startIdx: ref_path_endIdx + 1, :]
        else:
            ref_path_truncated = ref_path[ref_path_startIdx:ref_path_startIdx + 1 + path_length, :]
            ref_path_endIdx = ref_path_startIdx + path_length
    
    return ref_path_truncated,ref_path_startIdx,ref_path_endIdx


def calculate_dtw_similarity(traj_xy: np.ndarray, ref_path_xy: np.ndarray) -> float:

    distance, path = fastdtw(traj_xy, ref_path_xy, dist=euclidean)
    similarity_score = 1.0 / distance

    return similarity_score


def calculate_heading_alignment(traj_xy: np.ndarray, traj_heading: np.ndarray, ref: ref_path) -> float:

    from scipy.spatial.distance import cdist

    dist_matrix = cdist(traj_xy, ref.centerline_xy, metric='euclidean')
    matched_indices = np.argmin(dist_matrix, axis=1)
    matched_ref_heading = ref.centerline_h[matched_indices]

    heading_diff = traj_heading - matched_ref_heading
    heading_alignment = (np.abs(heading_diff))
    greater = np.any(heading_alignment > np.pi*60/180)

    return greater,heading_alignment[-1]

def calculate_similarity_score(traj_xy: np.ndarray, traj_h: np.ndarray, ref_path: ref_path) -> Tuple[float, float]:
    """Calculate the similarity score and heading alignment between a trajectory and a reference path.

    Args:
        traj_xy: Trajectory coordinates (shape: (N, 2))
        traj_h: Trajectory headings (shape: (N,))
        ref_path: Reference path

    Returns:
        similarity_score: Similarity score between the trajectory and reference path
        heading_alignment: Heading alignment between the trajectory and reference path
    """

    # mapped_traj_xy = map_trajectory_to_reference(traj_xy, ref_path.centerline_xy)
    nt_distance          = get_nt_distance(traj_xy, ref_path.centerline_xy)
    similarity_score     = -np.sum(np.abs(nt_distance[:,0]))

    greater,heading_alignment    = calculate_heading_alignment(traj_xy, traj_h, ref_path)
    if greater:
        similarity_score += 15
    return similarity_score, heading_alignment
    # return similarity_score, None
def calculate_similarity_score_hierarchical(traj_xy: np.ndarray, traj_h: np.ndarray, ref_path: ref_path) -> Tuple[float, float]:
    """Hierarchically calculate the similarity score and heading alignment between a trajectory and a reference path.

    Args:
        traj_xy: Trajectory coordinates (shape: (N, 2))
        traj_h: Trajectory headings (shape: (N,))
        ref_path: Reference path

    Returns:
        similarity_score: Combined score accounting for both distance and heading
        heading_alignment: Heading alignment between the trajectory and reference path
    """
    
    nt_distance = get_nt_distance(traj_xy, ref_path.centerline_xy)
    similarity_score = -np.sum(np.abs(nt_distance[:, 0]))

    # Heading alignment is initially set to None and only computed if needed
    heading_alignment = None
    
    if similarity_score >= -5:
        greater, heading_alignment = calculate_heading_alignment(traj_xy, traj_h, ref_path)
    
    return similarity_score, heading_alignment

def calculate_similarity_score_hierarchical(traj_xy: np.ndarray, traj_h: np.ndarray, ref_path: ref_path) -> Tuple[float, float]:
    """Hierarchically calculate the similarity score and heading alignment between a trajectory and a reference path.
    
    Args:
        traj_xy: Trajectory coordinates (shape: (N, 2))
        traj_h: Trajectory headings (shape: (N,))
        ref_path: Reference path

    Returns:
        similarity_score: Combined score accounting for both distance and heading
        heading_alignment: Heading alignment between the trajectory and reference path
    """
    
    nt_distance = get_nt_distance(traj_xy, ref_path.centerline_xy)
    similarity_score = -np.sum(np.abs(nt_distance[:, 0]))
    
    # Compute the heading alignment
    bad_heading_algin, heading_alignment = calculate_heading_alignment(traj_xy, traj_h, ref_path)
    
    # If heading alignment is more than 60 degrees, we assign an extremely low similarity score
    
    return similarity_score, heading_alignment, bad_heading_algin

def get_traj_closest_refs_with_similarity(traj_xy: np.ndarray, traj_h: np.ndarray, ref_path_list: List[ref_path], stationary: bool) -> List[ref_path]:
    """Get the centerlines from ref_path_list that are most aligned with the trajectory.
    
    Args:
        traj_xy: Trajectory coordinates (shape: (N, 2))
        traj_h: Trajectory headings (shape: (N,))
        ref_path_list: List of candidate reference paths
        stationary: Indicates if the car is stationary

    Returns:
        candidate_centerlines: Reference paths that are most aligned with the trajectory
    """
    assert len(ref_path_list)  > 0
    best_match = {
        'similarity_score': -float("inf"),
        'heading_alignment': float("inf"),
        'ref_path': None
    }

    for ref_path in ref_path_list:
        similarity_score, heading_alignment, bad_heading_align = calculate_similarity_score_hierarchical(traj_xy, traj_h, ref_path)
        
        if bad_heading_align and not stationary:
            similarity_score = -float("inf")
        
        # If the similarity score is less than -5, we check heading alignment 
        if similarity_score > -5:
            if heading_alignment < best_match['heading_alignment']:
                best_match['similarity_score'] = similarity_score
                best_match['heading_alignment'] = heading_alignment
                best_match['ref_path'] = ref_path
        else:
            if similarity_score > best_match['similarity_score']:
                best_match['similarity_score'] = similarity_score
                best_match['heading_alignment'] = heading_alignment
                best_match['ref_path'] = ref_path
            elif similarity_score == best_match['similarity_score']:
                # This part is ambiguous since we aren't appending but replacing.
                # If you have specific logic in mind for handling such cases, 
                # please let me know so I can adjust the code accordingly.
                pass

    if best_match['ref_path'] is None or best_match['similarity_score'] == -float("inf"):
        return [ref_path_list[0]]
    return [best_match['ref_path']]

def calculate_longitudinal_distance(xy_coordinates):
    distances = np.linalg.norm(np.diff(xy_coordinates, axis=0), axis=1)
    s = np.insert(np.cumsum(distances), 0, 0.0)
    return s


def get_normal_and_tangential_distance_point(
    x, y, centerline, start_idx, window_size, delta=0.01, last =False
):
    point = Point(x, y)
    centerline_ls = LineString(centerline)
    # Create a limited centerline within the sliding window
    end_idx = min(start_idx + window_size, len(centerline) - 1)
    if end_idx - start_idx > 0:
        limited_centerline = LineString(centerline[start_idx:end_idx+1])
    else:
        if end_idx == len(centerline) - 1:  # At the end of the trajectory
            limited_centerline = LineString(centerline[-2:])
    # limited_centerline = LineString(centerline[start_idx:end_idx+1])
    
    # Project the point onto the limited centerline
    tang_dist_limited = limited_centerline.project(point)
    norm_dist_limited = point.distance(limited_centerline)
    point_on_limited_cl = limited_centerline.interpolate(tang_dist_limited)
    
    # Calculate the distance on the original centerline
    distance_to_start_idx = centerline_ls.project(Point(centerline[start_idx]))
    tang_dist = distance_to_start_idx + tang_dist_limited
    
    # Find the new index on the original centerline
    new_idx = np.argmin(np.linalg.norm(centerline - np.array(point_on_limited_cl.coords[0]), axis=1))
    
    # Additional code to determine the direction (left/right of the line)
    point_on_cl = centerline_ls.interpolate(tang_dist)
     # Deal with last coordinate differently. Helped in dealing with floating point precision errors.
    if not last:
        pt1 = point_on_cl.coords[0]
        pt2 = centerline_ls.interpolate(tang_dist + delta).coords[0]
        pt3 = point.coords[0]

    else:
        pt1 = centerline_ls.interpolate(tang_dist - delta).coords[0]
        pt2 = point_on_cl.coords[0]
        pt3 = point.coords[0]
   
    
    lr_coords = [pt1, pt2, pt3]
    lr = LinearRing(lr_coords)
    
    if lr.is_ccw:
        return (tang_dist, norm_dist_limited, new_idx)
    return (tang_dist, -norm_dist_limited, new_idx)

from scipy.interpolate import CubicSpline

def fit_cubic_splines(xy_arr: np.ndarray, t_arr: np.ndarray, piece_wise_num=3) -> np.ndarray:
    """
    Return cubic spline coefficients for continuous piece-wise segments.

    Args:
        xy_arr (np.ndarray): Array of (x, y) points for the trajectory.
        t_arr (np.ndarray): Array of corresponding t (parameter) values.
        piece_wise_num (int): Number of piece-wise segments.

    Returns:
        tuple: A tuple containing the cubic spline coefficients for each segment and the t-boundaries.
    """
    segment_length = xy_arr.shape[0] // piece_wise_num
    spline_coeffs = []
    t_boundaries = []

    for i in range(piece_wise_num):
        start_idx = i * segment_length
        end_idx = (i + 1) * segment_length + 1 if i != piece_wise_num - 1 else xy_arr.shape[0]
        segment_xy = xy_arr[start_idx:end_idx]
        segment_t = t_arr[start_idx:end_idx]

        # Create cubic spline interpolations for x and y coordinates
        spline_x = CubicSpline(segment_t, segment_xy[:, 0], bc_type='natural')
        spline_y = CubicSpline(segment_t, segment_xy[:, 1], bc_type='natural')

        # Extract the polynomial coefficients of the cubic splines for x and y coordinates
        coeff_x = np.vstack((spline_x.c[::-1], spline_x.x[:-1]))  # Flip the coefficients to align with standard polynomial order
        coeff_y = np.vstack((spline_y.c[::-1], spline_y.x[:-1]))  # Flip the coefficients to align with standard polynomial order

        # Combine the x and y coefficients
        spline_coeffs.append(np.hstack((coeff_x, coeff_y)))

        # Store the t boundary of the segment
        t_boundaries.append(t_arr[end_idx - 1])

    return np.array(spline_coeffs), np.array(t_boundaries)

def fit_polynomials(xy_arr: np.ndarray, t_arr: np.ndarray , deg =4, piece_wise_num = 3) -> np.ndarray:
    ''' Return polynomial from high deg to low deg, [a3,a2,a1,a0]
    '''
    segment_length = xy_arr.shape[0] // piece_wise_num
    poly_coeffs = []
    t_boundaries = []
    for i in range(piece_wise_num):
        start_idx = i * segment_length
        end_idx = (i + 1) * segment_length + 1 if i != piece_wise_num - 1 else xy_arr.shape[0]
        segment_xy = xy_arr[start_idx:end_idx]
        segment_t = t_arr[start_idx:end_idx]

        if i > 0:
            segment_t = segment_t - t_boundaries[-1]

        coeff_x = np.polyfit(segment_t, segment_xy[:, 0], deg=deg)
        coeff_y = np.polyfit(segment_t, segment_xy[:, 1], deg=deg)

        poly_coeffs.append(np.vstack((coeff_x, coeff_y)))
        t_boundaries.append(t_arr[end_idx-1])
    return np.array(poly_coeffs), np.array(t_boundaries)


def transform_FRT2Cart_poly(nt_arr: np.ndarray, poly_coeffs: np.ndarray, t_boundaries: np.ndarray) -> np.ndarray:
    """Transform 2D nt array from Frenet Serret coordinates to Cartesian space using piece-wise polynomials."""
    t_arr = nt_arr[:, 1:]
    n_arr = nt_arr[:, 0:1]

    t_sqr_arr = np.hstack([t_arr**4, t_arr**3, t_arr**2, t_arr, np.ones_like(t_arr)])
    xy_arr = np.zeros_like(nt_arr)

    for i in range(len(t_boundaries)):
        if i == 0:
            segment_mask = nt_arr[:, 1] <= t_boundaries[i]
        elif i == len(t_boundaries) - 1:
            segment_mask = nt_arr[:, 1] > t_boundaries[i - 1]
        else:
            segment_mask = (nt_arr[:, 1] > t_boundaries[i - 1]) & (nt_arr[:, 1] <= t_boundaries[i])

        segment_poly_coeffs = poly_coeffs[i]
        segment_t_sqr_arr = t_sqr_arr[segment_mask]

        # Calculate the polynomial coefficients for x and y
        coeff_x, coeff_y = segment_poly_coeffs[0], segment_poly_coeffs[1]

        # Calculate the gradient of the polynomial coefficients
        grad_coeff_x = np.polyder(coeff_x)
        grad_coeff_y = np.polyder(coeff_y)

        # Calculate the tangent vector coefficients
        t_coeff = np.vstack((grad_coeff_x, grad_coeff_y))

        # Calculate the normal vector coefficients
        d_coeff = np.array([[0, -1], [1, 0]]) @ t_coeff

        # Offset the xy_arr by the normal vector coefficients
        d_arr = d_coeff @ segment_t_sqr_arr[:, 1:].T

        # Normalize the d_arr
        d_arr /= np.linalg.norm(d_arr, axis=0)

        xy_segment_arr = (segment_poly_coeffs @ segment_t_sqr_arr.T).T
        xy_segment_arr += n_arr[segment_mask] * d_arr.T

        xy_arr[segment_mask] = xy_segment_arr

    return xy_arr

def transform_FRT2Cart(nt_arr: np.ndarray,centerline: np.ndarray, centerline_s: np.ndarray)->np.ndarray:
    '''Transform 2d nt array from frenet serret coordinates cartisian space 
    '''
    xy_arr = np.apply_along_axis(FRT2Cart,axis = 1,arr = nt_arr,centerline = centerline, centerline_s=centerline_s)
    return xy_arr

def FRT2Cart(point_nt, centerline, centerline_s) -> np.ndarray:
    d,s = point_nt[0],point_nt[1]
    idx2 = np.argmax(centerline_s>s) #find the segment
    idx = idx2 - 1
    if idx <0:
        idx,idx2 = 0,1
    if centerline_s[idx]-centerline_s[idx2]==0:
        idx-=1
        
    ref_vec = centerline[idx2] - centerline[idx]
    fHeading = np.arctan2(ref_vec[1],ref_vec[0])
    # The x,y,s along the segment
    fSegmentS = s - centerline_s[idx]

    fSegmentX = centerline[idx][0] + fSegmentS * np.cos(fHeading)
    fSegmentY = centerline[idx][1] + fSegmentS * np.sin(fHeading)

    fPerpendicularHeading = fHeading + np.pi/2
    
    x = fSegmentX + d * np.cos(fPerpendicularHeading)
    y = fSegmentY + d * np.sin(fPerpendicularHeading)
 
    return np.hstack((x,y))


def get_nt_distance(xy: np.ndarray, centerline: np.ndarray, window_size = 50) -> np.ndarray:
    """Get normal (offset from centerline) and tangential (distance along centerline) distances for the given xy trajectory,
    along the given centerline.

    Args:
        xy: Sequence of x,y,z coordinates.
        centerline: centerline along which n-t is to be computed

    Returns:
        nt_distance: normal (offset from centerline) and tangential (distance along centerline) distances.
    """
    traj_len = xy.shape[0]
    nt_distance = np.zeros((traj_len, 2))

    max_dist: float = -1
    
    start_idx = np.argmin(np.linalg.norm(centerline - xy[0], axis=1))

    for i in range(traj_len):
        end_idx =  min(len(centerline), start_idx + window_size)
        tang_dist, norm_dist,new_idx = get_normal_and_tangential_distance_point(
            xy[i][0], xy[i][1], centerline,start_idx,window_size, last=False)

        # Keep track of the last coordinate
        if tang_dist > max_dist:
            max_dist = tang_dist
            last_x = xy[i][0]
            last_y = xy[i][1]
            last_idx = i
        nt_distance[i, 0] = norm_dist
        nt_distance[i, 1] = tang_dist
        start_idx = new_idx

    tang_dist, norm_dist,_ = get_normal_and_tangential_distance_point(last_x, last_y, centerline,start_idx,window_size, last=True)
    nt_distance[last_idx, 0] = norm_dist

    return nt_distance


def discretize_lane_into_n_pts(centerline: np.ndarray, n_value: int) -> np.ndarray:
    """
    Discretize centerline into 'n_value' equidistant points. 
    :param centerline: centerline as np array
    :param n_value: number of coordinates to discretize it as
    """
    centString = LineString(centerline)
    normalized_dist_to_interp = np.linspace(0,1, num=n_value).tolist()
    new_centerline = []
    for dist in normalized_dist_to_interp:
        interPts = centString.interpolate(dist, normalized=True)
        new_centerline.append(list(interPts.coords)[0])
    new_centerline = np.array(new_centerline)
    
    return new_centerline

def chop_lane_for_trajectory(centerline: np.ndarray, xy: np.ndarray, meters_before: float, meters_after: float, num_pts:int = 150) -> np.ndarray:
    """
    Chop lanes before and after the starting point of trajectory based on given meters before and after that point.
    :param meters_before: meters before we chop off lane from xy[-1]
    :param meters_after: meters after we chop off lane from xy[-1]
    :param num_pts: Number of points to chop the lane between the meters before and meters_after
    :return chopped of lane
    """

    centerline_ls = LineString(centerline)
    point = Point(xy[-1,0], xy[-1,1])
    tang_dist = centerline_ls.project(point)
    # dist_to_interp = np.arange(tang_dist - meters_before, tang_dist + meters_after, 1.0).tolist()

    # Identify lane start and end point 
    start_pt = tang_dist - meters_before
    if start_pt < 0:
        start_pt = 0
    end_pt = tang_dist + meters_after
    if end_pt > centerline_ls.length:
        end_pt = centerline_ls.length

    dist_to_interp = np.linspace(start_pt, end_pt, num=num_pts).tolist()
    new_centerline = []
    for dist in dist_to_interp:
        interPts = centerline_ls.interpolate(dist)
        new_centerline.append(list(interPts.coords)[0])
    new_centerline = np.array(new_centerline)

    # new_cent_length = LineString(new_centerline).length
    # print(new_cent_length, new_centerline.shape)
    # if new_cent_length < 100:
    #     print(new_centerline)
    #     print('--')
    # Compute heading
    delta_x = np.diff(new_centerline[:, 0])
    delta_y = np.diff(new_centerline[:, 1])
    heading = np.arctan2(delta_y, delta_x)

    # Duplicate the last heading to make it the same size as new_centerline
    heading = np.append(heading, heading[-1])
    return new_centerline, heading

def correct_centerline_and_nt_dist(past_nt_dist, future_nt_dist, past_pixel_traj, future_pixel_traj, centerline, num_pts=150):
    """
    fixes the centerline and trajectory such that centerline does not start after the trajectory (log dist > 0 for the first point)
    """

    xy = np.concatenate([past_pixel_traj, future_pixel_traj], axis=0)
    nt = np.concatenate([past_nt_dist, future_nt_dist], axis=0)
    centString = LineString(centerline)
    ind_before_centerline = np.where(nt[:,1]<=0.0)
    ind_after_centerline = np.where(nt[:,1]>=centString.length)
    
    # print(ind_after_centerline[0].shape, ind_after_centerline[0].shape)
    if ind_before_centerline[0].shape[0] == 0 and ind_after_centerline[0].shape[0] == 0:
        return past_nt_dist, future_nt_dist, centerline
    
    if ind_before_centerline[0].shape[0] == nt.shape[0]:
        centerline = xy
        # print('Centerline only starts after trajectory ')
    elif ind_before_centerline[0].shape[0]>0:
        centerline = np.concatenate([xy[ind_before_centerline], centerline], axis=0)

    if ind_after_centerline[0].shape[0] == nt.shape[0]:
        centerline = xy
        # print('Centerline ends before trajectory ')

    elif ind_after_centerline[0].shape[0] > 0:
        centerline = np.concatenate([centerline, xy[ind_after_centerline]], axis=0)
    
    centerline = discretize_lane_into_n_pts(centerline, num_pts)

    past_nt_dist = get_nt_distance(past_pixel_traj, centerline)
    future_nt_dist  = get_nt_distance(future_pixel_traj, centerline)
    #print(past_nt_dist)
    # print('--')

    return past_nt_dist, future_nt_dist, centerline

def prune_centerlines_for_trajectory(xy, candidate_cl: List[np.ndarray], projection_time=6.0):
    """
    Removes redudant lane centerlines which have the same end point at some lookahead distance.
    """

    if xy.shape[0]>1:
        xy_ls = LineString(xy)
        xy_length = xy_ls.length
        veh_speed = xy_length/(xy.shape[0]*0.1)
    else:
        veh_speed = 20.0
    
    # Look ahead distance
    projected_dist = veh_speed*projection_time

    cent_lk_list = []
    traj_dist_in_lane = []
    cent_lengths = []
    for centerline in candidate_cl:
        point = Point(xy[-1,0], xy[-1,1])
        centerline_ls = LineString(centerline)
        cent_lengths.append(centerline_ls.length)
        tang_dist = centerline_ls.project(point)
        traj_dist_in_lane.append(tang_dist)
        look_ahead_pt = centerline_ls.interpolate(tang_dist + projected_dist)
        cent_lk_list.append(list(look_ahead_pt.coords)[0])


    cent_lk_arr = np.array(cent_lk_list)
    lk_pt_cent_dist = np.linalg.norm(cent_lk_arr[np.newaxis] - cent_lk_arr[:, np.newaxis], axis=-1)
    same_cent = np.zeros(cent_lk_arr.shape[0], dtype=int)
    
    identical_count = 1
    for i in range(lk_pt_cent_dist.shape[0]):
        identical_cent_ind = np.where(lk_pt_cent_dist[i] < 0.5)
        if not np.all(same_cent[identical_cent_ind]):
            same_cent[identical_cent_ind] = identical_count
            identical_count += 1

    uniq_cent_id = np.unique(same_cent).tolist()
    pruned_centerlines = []
    for cent_id in uniq_cent_id:
        same_lane_ind = np.where(same_cent == cent_id)
        if same_lane_ind[0].shape[0] < 1:
            continue

        pruned_centerlines.append(candidate_cl[same_lane_ind[0][0]])
        diff_cent_ind = np.where(same_cent != cent_id)

        traj_dist_pick = np.array(traj_dist_in_lane)[same_lane_ind[0]]
        lane_dist_pick = np.array(cent_lengths)[same_lane_ind[0]]

        lk_lengths = lane_dist_pick - traj_dist_pick
        max_lk_length = np.amax(lk_lengths)
        biggest_lane = np.argmax(lk_lengths)
        big_lane_ind = same_lane_ind[0][biggest_lane]
        for diff_ind in diff_cent_ind[0].tolist():
            lk_dist = cent_lengths[diff_ind] - traj_dist_in_lane[diff_ind]
            if not max_lk_length < lk_dist:
                big_lane_lk_pt = np.array(list(LineString(candidate_cl[big_lane_ind]).interpolate(traj_dist_in_lane[big_lane_ind] + lk_dist).coords))
                diff_ind_lk_pt = np.array(list(LineString(candidate_cl[diff_ind]).interpolate(traj_dist_in_lane[diff_ind] + lk_dist).coords))
                dist_btwn_pts = np.linalg.norm(big_lane_lk_pt - diff_ind_lk_pt)
                if dist_btwn_pts < 0.5:
                    same_cent[diff_ind] = cent_id


    

    # print(lk_pt_cent_dist.shape)
    # print(veh_speed, cent_lk_list)
    # if viz:
    #     plt.figure(0, figsize=(8, 7))
    #     for centerline_coords in pruned_centerlines:
    #         visualize_centerline(centerline_coords)
    #     plt.plot(
    #         xy[:, 0],
    #         xy[:, 1],
    #         "-",
    #         color="#d33e4c",
    #         alpha=1,
    #         linewidth=3,
    #         zorder=15,
    #     )

    #     final_x = xy[-1, 0]
    #     final_y = xy[-1, 1]

    #     plt.plot(
    #         final_x,
    #         final_y,
    #         "o",
    #         color="#d33e4c",
    #         alpha=1,
    #         markersize=10,
    #         zorder=15,
    #     )
    #     plt.xlabel("Map X")
    #     plt.ylabel("Map Y")
    #     plt.axis("off")
    #     plt.title(f"Number of candidates = {len(candidate_cl)}")
    #     plt.show()


    return same_cent


def get_xy_from_nt_seq(nt_seq: np.ndarray, centerlines: List[np.ndarray]) -> np.ndarray:
    ''' Convert n-t coordinates to x-y, i.e. , convert from centerline curvilinear coordinates to map coordinates
    Args:
        nt_seq (np.array): Array of shape (num_tracks * seq * 2) where last dimension has 'n' offset from centerline and 't' (distance along centerlines)
        centerlines (list of np.array): Cebterkube fir eacg track
    Returns:
        xy_seq (nump array): Array of shape (num_tracks * seq_len *2) where last dimension contains coordinates in map frame
    '''
    seq_len = nt_seq.shape[1]
    #coordinates obtained by interpolating distances on the centerline
    xy_seq = np.zeros(nt_seq.shape)

    for i in range(nt_seq.shape[0]):
        curr_cl = centerlines[i]
        line_string = LineString(curr_cl)
        length_ls = line_string.length

        for time in range(seq_len):

            #project nt to xy
            offset_from_cl = nt_seq[i][time][0]
            dist_along_cl =  nt_seq[i][time][1]

            if dist_along_cl<=length_ls:
                x_coord, y_coord = get_xy_from_nt(offset_from_cl, dist_along_cl, curr_cl)
            else:
                x_coord, y_coord = -1, -1
            
            xy_seq[i, time, 0] = x_coord
            xy_seq[i, time ,1] = y_coord

        return xy_seq
    
def get_xy_from_nt(n: float, t: float, centerline: np.ndarray) -> Tuple[float,float]:
    '''Convert a single n-t coordinate (cenerline curvilinear coordinate) to absolute x-y
    Args:
        n (float): Offset from centerline
        t (float): Distance along the centerline

    Returns:
    x1 (float): x-coordinate in map frame
    y1 (float): y-coordinate in map frame    
    '''
    line_string = LineString(centerline)

    # If the distance along centerline is negative , keep it to the start of line
    point_on_cl = line_string.interpolate(t) if t > 0 else line_string.interpolate(0)
    local_ls = None

    #Find 2 consective points on centerline such that line joining those 2 points
    # contains point_on_cl

    for i in range(len(centerline)-1):
        pt1 = centerline[i]
        pt2 = centerline[i+1]
        ls = LinearRing([pt1,pt2])
        if ls.distance(point_on_cl)< 1e-8:
            local_ls = ls
            break
    
    assert local_ls is not None, "XY from N({} T{}) not computed correctly".fromat(n,t)

    pt1, pt2 = local_ls.coords[:]
    x0, y0   = point_on_cl.coords[0]

    # Determine whether the coordinate lies on lef or right side of the line formed by pt1 and pt2
    #Find a point on either of the line
    #If the ring formed by (pt1,pt2, (x1_1,y1_1)) is counter clock wise, then it lies on the left

    #Edge cases
    #Vertical
    if pt1[0] ==pt2[0]:
        m = 0
        x1_1, x1_2 = x0 + n, x0 - n
        y1_1, y1_2 = y0, y0
    # Horizontal
    elif pt1[1] == pt2[1]:
        m = float("inf")
        x1_1, x1_2 = x0 , x0 
        y1_1, y1_2 = y0 + n, y0 - n
    #general case
    else:
        ls_slope = (pt2[1]-pt1[1])/(pt2[0]-pt1[0])
        m  = -1 / ls_slope

        x1_1 = x0 + n/ math.sqrt(1+ m**2)
        y1_1 = y0 + m*(x1_1 - x0)
        x1_2 = x0 - n / math.sqrt(1+ m**2)
        y1_2 = y0 + m* (x1_2 - x0)

    #Rings fomed by pt1 pt2 and coordinates computed above
    lr1 = LinearRing([pt1, pt2, (x1_1, y1_1)])
    lr2 = LinearRing([pt1, pt2, (x1_2, y1_2)])

    #If ring is counter clockwise

    if lr1.is_ccw:
        x_ccw, y_ccw = x1_1, y1_1
        x_cw, y_cw   = x1_2, y1_2
    else:
        x_ccw, y_ccw = x1_2, y1_2
        x_cw, y_cw   = x1_1, y1_1


    # If offset is positive, coordinate on the left
    if n > 0:
        x1, y1 = x_ccw, y_ccw
    else:
        x1, y1 = x_cw, y_cw

    return x1, y1