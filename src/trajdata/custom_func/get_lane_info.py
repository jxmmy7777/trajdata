import random
from collections import defaultdict

from typing import Any, Dict, Iterable, List, Mapping, Optional, Sequence, Tuple, Union, Callable
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm
import json

from trajdata import AgentBatch, AgentType, UnifiedDataset
from trajdata.data_structures.batch_element import AgentBatchElement
from trajdata.maps import VectorMap
from trajdata.maps.vec_map_elements import RoadLane
from trajdata.maps.ref_utils import ref_path
from trajdata.maps.centerline_utils import *

from trajdata.utils.arr_utils import transform_angles_np, transform_coords_np
from trajdata.utils.state_utils import transform_state_np_2d
from trajdata.visualization.vis import plot_agent_batch

import random
from collections import defaultdict

from typing import Any, Dict, Iterable, List, Mapping, Optional, Sequence, Tuple, Union, Callable
import matplotlib.pyplot as plt
import numpy as np
import torch
from torch.utils.data import DataLoader
from tqdm import tqdm
import json

from trajdata import AgentBatch, AgentType, UnifiedDataset
from trajdata.data_structures.batch_element import AgentBatchElement
from trajdata.maps import VectorMap
from trajdata.maps.vec_map_elements import RoadLane
from trajdata.maps.ref_utils import ref_path
from trajdata.maps.centerline_utils import *
from trajdata.utils.arr_utils import transform_angles_np, transform_coords_np
from trajdata.utils.state_utils import transform_state_np_2d
from trajdata.visualization.vis import plot_agent_batch


def get_lane_info(element: AgentBatchElement, num_future_lanes=6, ref_polyline_ids="",num_polylines = 10) -> np.ndarray:
    """Closest lane for predicted agent."""

    # Transform from agent coordinate frame to world coordinate frame.
    vector_map: VectorMap = element.vec_map
        
    total_hist_fut_horizon = int((element.future_sec[0]+element.history_sec[0])/element.dt)+1
    hist_len = int(np.round(element.history_sec[0]/element.dt))
    fut_len  = int(np.round(element.future_sec[0]/element.dt))
    world_from_agent_tf = np.linalg.inv(element.agent_from_world_tf)
    if (len(element.agent_future_np)<=0 and ref_polyline_ids=="") or ref_polyline_ids=="None":
        # print("-----------No future info ---------------")
        return_dict = {
        'all_poss_refs': np.full((num_future_lanes,150,2),0),
        'centerline_xy': np.full((150,2),0),
        'left_centerline_xy':  np.full((150,2),0),
        'right_centerline_xy': np.full((150,2),0),
        'centerline_s': np.full((150,),0),
        'nt_arr_shift': np.full((total_hist_fut_horizon,2),0),
        'nt_delta_target':np.full((fut_len,2),0),
        'nt_delta_hist': np.full((hist_len,2),0),
        "delta_nt_arr":np.full((total_hist_fut_horizon-1,2),0),
        "ref_polyline_ids": "None",
        'right_ref_polyline_ids': "None",
        'left_ref_polyline_ids': "None",

        "nt_current":np.full((2,),0),
        "polynomial_coeff": np.zeros((num_polylines,2,5)),
        "t_boundaries":np.zeros((num_polylines,)),
        'has_lane':False,
        }
        return return_dict
    

    agent_past_xyzh_world = transform_state_np_2d(
        element.agent_history_np, world_from_agent_tf
    ).as_format("x,y,z,h")
    point_xyzh = agent_past_xyzh_world[-1]

    if len(element.agent_future_np)<1:
        agent_future_xyzh_world = np.full((fut_len,4),np.nan)
        test_path = agent_past_xyzh_world
    else:
        agent_future_xyzh_world = transform_state_np_2d(
            element.agent_future_np, world_from_agent_tf
        ).as_format("x,y,z,h")
        test_path = np.vstack([agent_past_xyzh_world,agent_future_xyzh_world])




    # Use cached kdtree to find closest lane point
    
    ## Find the closest lane if no ref_polylines_id
    if ref_polyline_ids == "":
        dist_threshold = 5.0
        possible_lanes = vector_map.get_current_lane(point_xyzh, max_dist=dist_threshold)
        while len(possible_lanes) < 1 and dist_threshold < 10:
            dist_threshold *= 2.0
            possible_lanes = vector_map.get_current_lane(point_xyzh, max_dist=dist_threshold)
        # if len(possible_lanes) < 1:
        #     possible_lanes = vector_map.get_closest_unique_lanes(agent_future_xyzh_world[:,:3])



        # assert len(possible_lanes) > 0, "No nearby lanes found!!"
        if len(possible_lanes) <1:
            # print("-----------No nearby lanes found!!----------------")
            return_dict = {
            'all_poss_refs': np.full((num_future_lanes,150,2),0),
            'centerline_xy': np.full((150,2),0),
            'left_centerline_xy':  np.full((150,2),0),
            'right_centerline_xy': np.full((150,2),0),
            'centerline_s': np.full((150,),0),
            'nt_arr_shift': np.full((total_hist_fut_horizon,2),0),
            'nt_delta_target':np.full((fut_len,2),0),
            'nt_delta_hist': np.full((hist_len,2),0),
            "delta_nt_arr":np.full((total_hist_fut_horizon-1,2),0),
            "nt_current":np.full((2,),0),
            "ref_polyline_ids": "None",
            'right_ref_polyline_ids': "None",
            'left_ref_polyline_ids': "None",

            "polynomial_coeff": np.zeros((num_polylines,2,5)),
            "t_boundaries":np.zeros((num_polylines,)),
            'has_lane':False,
            }
            return return_dict


        obs_pred_lanes : List[Sequence[str]] = []

        
        ## apply dfs on all possible lanes and stitch
        for lane in possible_lanes:
            candidates_future = dfs(lane, vector_map, dist = 0, threshold=30)
            candidates_past   = dfs(lane, vector_map, dist = 0, threshold=20, extend_along_predecessor=True)
            
            # Merge past and future TODO Somehow the code is reversedm si the candidate _past is the future
            for past_lane_seq in candidates_past:
                for future_lane_seq in candidates_future:
                    assert past_lane_seq[-1] == future_lane_seq[0], "Incorrect DFS for candidate lanes past and future"
                    obs_pred_lanes.append(past_lane_seq + future_lane_seq[1:])

        ##remove_overlapping_lane_seq
        obs_pred_lanes = remove_overlapping_lane_seq(obs_pred_lanes)
        ## Create reference trajectories based on candidates
        ref_list = []
        for pred_lanes in obs_pred_lanes:
            ref_list.append(ref_path(pred_lanes))


        #Reduce the number of candidates based on distance travelled along the centerline
        if len(ref_list) >1 and len(agent_future_xyzh_world[:,:2]) >1:
            # candidate_refs = filter_candidate_centerlines(agent_future_xyzh_world[:,:2], ref_list)
            candidate_refs = get_traj_closest_refs_with_similarity(test_path[:,:2],test_path[:,-1:], ref_list,element.agent_meta_dict["is_stationary"])
        else:
            candidate_refs = ref_list
        
        if len(candidate_refs) < 1:
            candidate_refs = get_centerlines_most_aligned_with_trajectory(agent_future_xyzh_world[:,:2], ref_list)
            # candidate_refs = get_traj_closest_refs_with_similarity(test_path[:,:2],test_path[:,-1:], candidate_refs,element.agent_meta_dict["is_stationary"])
        
        assert len(candidate_refs) >= 1
        closest_ref: ref_path = candidate_refs[0]
        cloests_ref_polyline_ids = closest_ref.polyline_ids

        # store all future possible lanes for visualization or future usage
        all_poss_refs = np.full((num_future_lanes,150,2),np.nan)
        for i,ref in enumerate(ref_list):
            if i >=6:
                break
            chopped_center_line_candidate,_ = chop_lane_for_trajectory(ref.centerline_xy,test_path[:element.agent_history_len,:2],meters_before=30,meters_after=30)
            agent_centric_chopped_center_line_candidate = transform_coords_np(
                                                             chopped_center_line_candidate, element.agent_from_world_tf
                                                            ) 
            all_poss_refs[i] = agent_centric_chopped_center_line_candidate
        all_poss_refs = all_poss_refs[:num_future_lanes]

    else:
        all_poss_refs = np.full((num_future_lanes,150,2),np.nan)
        polyline_ids =  json.loads(ref_polyline_ids)
        cloests_ref_polyline_ids = ref_polyline_ids
        pred_lanes = [vector_map.get_road_lane(id) for id in polyline_ids]
        ## TODO, can extend reference path if needed
        closest_ref: ref_path = ref_path(pred_lanes)
        extend_future_lane = closest_ref.check_extend_future_lane(test_path[[element.agent_history_len-1],:2])

        # extend_future_lane = closest_ref.extend_future_lane(test_path[:,:2])
        if extend_future_lane and len(list(closest_ref.last_lane.next_lanes))>0:
            next_lane_id = list(closest_ref.last_lane.next_lanes)[0]
            next_lane = vector_map.get_road_lane(next_lane_id)
            next_lane_list = dfs(next_lane, vector_map, dist = 0, threshold=50)[0] #randomly pick one
            closest_ref.extend_future_lane(next_lane_list)
            #TODO we shouldn't fit polynomials again but add it on?
        

    # Add changing lane information for closed-loop simulation
    # Find currenct Road lane?

    cur_lane = closest_ref.get_current_lane(test_path[:element.agent_history_len,:2])
    #For each lane add adjacent lanes? Just add it in current left right lane?
    # get left/right lanes

    left_lane,right_lane = find_adjacent_lanes(cur_lane,vector_map)
    agent_centric_left_centerline_xy = np.full((150,2),0)
    agent_centric_right_centerline_xy = np.full((150,2),0)
    right_ref = None
    left_ref  = None
    # #Note: In simulation, if they switch the lane, then use that as the new ref_lane!
    # if left_lane is not None:
    #     # DFS to get the full reference of that lane
    #     next_lane_list = dfs(left_lane, vector_map, dist=0, threshold=150)[0] # Randomly pick one
    #     left_ref: ref_path = ref_path(next_lane_list)
    #     chopped_left_center_line_xy,_ = chop_lane_for_trajectory(
    #         left_ref.centerline_xy,
    #         test_path[:element.agent_history_len,:2],
    #         meters_before=50, 
    #         meters_after=150)
        
    #     agent_centric_left_centerline_xy = transform_coords_np(
    #         chopped_left_center_line_xy, element.agent_from_world_tf
    #     )

    # if right_lane is not None:
    #     # DFS to get the full reference of that lane
    #     next_lane_list = dfs(right_lane, vector_map, dist=0, threshold=150)[0] # Randomly pick one
    #     right_ref: ref_path = ref_path(next_lane_list)
    #     chopped_right_center_line_xy,_ = chop_lane_for_trajectory(
    #         right_ref.centerline_xy,
    #         test_path[:element.agent_history_len,:2],
    #         meters_before=50, 
    #         meters_after=150)
        
    #     agent_centric_right_centerline_xy = transform_coords_np(
    #         chopped_right_center_line_xy, element.agent_from_world_tf
    #     )
      
    # if right_lane is not None:
    #     agent_centric_right_centerline_xy = transform_coords_np(
    #             right_lane.center.xy, element.agent_from_world_tf
    #     )

    extened_center_line_xy , _ = check_and_extend_centerline(test_path, closest_ref.centerline_xy, closest_ref.centerline_h, some_distance_threshold=5, some_heading_threshold=np.pi/8)

    ## Transformation nt
    chopped_center_line_xy, _ = chop_lane_for_trajectory(extened_center_line_xy,test_path[:element.agent_history_len,:2][:element.agent_history_len,:2],meters_before=50,meters_after=150)
    
    #check if the point does not reach the centerline yet
    centerline_s = calculate_longitudinal_distance(chopped_center_line_xy)
    
    #TODO handle roundabout cases!!
    nt_arr = get_nt_distance(test_path[:,:2],chopped_center_line_xy)
        
    agent_centric_chopped_center_line_xy = transform_coords_np(
        chopped_center_line_xy, element.agent_from_world_tf
    )
    agent_centric_test_path = transform_coords_np(
        test_path[:,:2], element.agent_from_world_tf
    )
        

    ## TODO Change to CubicPline
    poly_coeff,t_bound = fit_polynomials(agent_centric_chopped_center_line_xy,centerline_s,piece_wise_num=num_polylines)
    xy_from_nt = transform_FRT2Cart_poly(nt_arr, poly_coeff,t_bound)
    # n_violate,t_violate = check_continuous_and_bounded(nt_arr)
    # if n_violate or t_violate:
        
    #     plt.figure()
    #     plt.subplot(1, 2, 1)
    #     T = len(agent_centric_chopped_center_line_xy)  # Assuming time length is T

    #     # Create color maps
    #     time_colormap_centerline = np.arange(T)
    #     time_colormap_test_path = np.arange(len(agent_centric_test_path))

    #     plt.scatter(agent_centric_chopped_center_line_xy[:, 0], agent_centric_chopped_center_line_xy[:, 1], c=time_colormap_centerline, cmap='winter', label='Centerline')
    #     plt.scatter(agent_centric_test_path[:, 0], agent_centric_test_path[:, 1], c=time_colormap_test_path,marker="x", cmap='coolwarm', label='Test Path')

    #     plt.colorbar(label='Time')
    #     plt.legend()
    #     plt.title("Centerline and Test Path")
    #     plt.xlabel("X")
    #     plt.ylabel("Y")
    #     #set bound to nt_arr
    #     plt.xlim(-50,50)
    #     plt.ylim(-50,50)

    #     # Plotting nt coordinates
    #     plt.subplot(1, 2, 2)
    #     plt.scatter(nt_arr[:, 0], nt_arr[:, 1], c=np.arange(len(nt_arr)), cmap='coolwarm', label='NT Coordinates')
    #     plt.colorbar(label='Time')
    #     #set bound to nt_arr
    #     plt.xlim(-5,5)
    #     plt.ylim(0,100)
    #     plt.legend()
    #     plt.title("NT Coordinates")
    #     plt.xlabel("N")
    #     plt.ylabel("T")

    #     plt.tight_layout()
    #     plt.savefig(f"visualization/debug_plots{element.data_index}.png")
    #     plt.close()
    #     return_dict = {
    #         'all_poss_refs': np.full((num_future_lanes,150,2),0),
    #         'centerline_xy': np.full((150,2),0),
    #         'left_centerline_xy':  np.full((150,2),0),
    #         'right_centerline_xy': np.full((150,2),0),
    #         'centerline_s': np.full((150,),0),
    #         'nt_arr_shift': np.full((total_hist_fut_horizon,2),0),
    #         'nt_delta_target':np.full((fut_len,2),0),
    #         'nt_delta_hist': np.full((hist_len,2),0),
    #         "delta_nt_arr":np.full((total_hist_fut_horizon-1,2),0),
    #         "nt_current":np.full((2,),0),
    #         "ref_polyline_ids": "None",
    #         'right_ref_polyline_ids': "None",
    #         'left_ref_polyline_ids': "None",

    #         "polynomial_coeff": np.zeros((num_polylines,2,5)),
    #         "t_boundaries":np.zeros((num_polylines,)),
    #         'has_lane':False,
    #         }
    #     return return_dict
    

    ## Store nt info at current frame
    nt_curr = nt_arr[element.agent_history_len-1] #TODO figure out which frame
    nt_arr_shift  = nt_arr - nt_curr
    delta_nt_arr = np.diff(nt_arr, axis = 0)
    ## DEBUGGING for transformation
    # xy_from_nt = transform_FRT2Cart(nt_arr_shift+nt_curr, chopped_center_line_xy, centerline_s)
    # assert np.any(np.linalg.norm(test_path[:,:2]-xy_from_nt)<=1)
    # agent_centric_chopped_center_line_xy = xy_from_nt

    

    return_dict = {
        'all_poss_refs': all_poss_refs,

        'centerline_xy': agent_centric_chopped_center_line_xy,
        'left_centerline_xy': agent_centric_left_centerline_xy,
        'right_centerline_xy':agent_centric_right_centerline_xy,
        'right_ref_polyline_ids': json.dumps(right_ref.polyline_ids) if right_ref is not None else "None",
        'left_ref_polyline_ids': json.dumps(left_ref.polyline_ids) if left_ref is not None else "None",
        'centerline_s': centerline_s,

        'nt_arr_shift': create_np_arr_pad(nt_arr_shift,(total_hist_fut_horizon,2)), #shape total_len,2
        'nt_delta_target': create_np_arr_pad(delta_nt_arr[element.agent_history_len-1:],(fut_len,2)), #shape fut_len,2
        'nt_delta_hist':create_np_arr_pad(delta_nt_arr[:element.agent_history_len-1],(hist_len,2)) , #shape hist_len,2
        'delta_nt_arr': create_np_arr_pad(delta_nt_arr,(total_hist_fut_horizon-1,2)), #shape total_len-1,2
        'ref_polyline_ids': json.dumps(cloests_ref_polyline_ids),

        "nt_current":nt_curr,
        "polynomial_coeff": poly_coeff,
        "t_boundaries":t_bound,
        "has_lane": True,
    }
    return return_dict
def create_np_arr_pad(np_arr,padding_shape = (30,2)):
    np_arr_pad = np.full(padding_shape, 0.0)
    np_arr_pad[:len(np_arr)] = np_arr
    return np_arr_pad

def dfs(
    lane: Union[str,RoadLane],
    vector_map: VectorMap,
    dist: float = 0,
    threshold: float = 30,
    extend_along_predecessor: bool = False,
    ) -> List[List[str]]:
        """
        Perform depth first search over lane graph up to the threshold.

        Args:
            lane_id: Starting lane_id (Eg. 12345)
            city_name
            dist: Distance of the current path
            threshold: Threshold after which to stop the search
            extend_along_predecessor: if true, dfs over predecessors, else successors

        Returns:
            lanes_to_return (list of list of str): List of sequence of lane ids
                Eg. [[12345, 12346, 12347], [12345, 12348]]

        """
        if isinstance(lane,str):
            lane =  vector_map.get_road_lane(lane)
        
        if dist > threshold:
            return [[lane]]
        else:
            traversed_lanes = []
            try:    
                child_lanes = (
                    lane.next_lanes
                    if not extend_along_predecessor
                    else lane.prev_lanes
                )
            except:
                child_lanes = None
            if child_lanes is not None:
                for child in child_lanes:
                    if isinstance(child,str):
                        child =  vector_map.get_road_lane(child)
                    centerline = child.center
                    cl_length = centerline.total_distance
                    curr_lane_ids = dfs(child, vector_map, dist + cl_length, threshold, extend_along_predecessor)
                    traversed_lanes.extend(curr_lane_ids)
            if len(traversed_lanes) == 0:
                return [[lane]]
            lanes_to_return = []
            for lane_seq in traversed_lanes:
                lanes_to_return.append(lane_seq + [lane] if extend_along_predecessor else [lane] + lane_seq)
            return lanes_to_return

# find adjacent lanes
from scipy.spatial.distance import cdist

def find_adjacent_lanes(lane, vec_map):
    mid, end =lane.center.midpoint, lane.center.points[-1]
    
    # Get possible lanes for start, mid, and end points
    possible_lanes_mid = vec_map.get_current_lane(mid, max_dist=3.5)
    possible_lanes_end = vec_map.get_current_lane((end+mid)/2, max_dist=3.5)
    
    # Combine and deduplicate the results
    possible_lanes = list(set(possible_lanes_mid + possible_lanes_end))
    
    # remove itself
    if lane in possible_lanes:
        possible_lanes.remove(lane)
        
    # remove successors or predecessor
    lanes_to_remove = [lane_candidate for lane_candidate in possible_lanes 
                       if lane_candidate.id in lane.prev_lanes or 
                       lane_candidate.id in lane.next_lanes]
    
    for lane_to_remove in lanes_to_remove:
        possible_lanes.remove(lane_to_remove)
    
    closest_left_lane, closest_right_lane =  get_adjacent_lanes(lane, possible_lanes)
    # plot_lanes(lane, closest_left_lane, closest_right_lane)

    return closest_left_lane,closest_right_lane



def get_adjacent_lanes(target_lane, all_lanes, width=3.5):
    polyline = target_lane.center.points[:, :2]  # Extract the first two dimensions as coordinates
    headings = target_lane.center.points[:, 3]  # Extract the heading
    # Convert the headings into normals
    normals = np.column_stack([np.sin(headings), -np.cos(headings)])

    left_buffers = polyline + width * normals
    right_buffers = polyline - width * normals
    # plot_lanes(polyline,left_buffers,right_buffers)
    closest_left_lane = find_closest_lane(left_buffers, normals, all_lanes)
    closest_right_lane = find_closest_lane(right_buffers, -normals, all_lanes,right=True)
   
    return closest_left_lane, closest_right_lane

def find_closest_lane(buffers, normals, all_lanes, right=False):
    min_distance = 3.5
    closest_lane = None

    for lane in all_lanes:
        # Assuming the lane is also represented as an np array
        polyline = lane.center.points[:, :2]
        headings = lane.center.points[:, 3]
        lane_dir = np.column_stack([np.sin(headings), -np.cos(headings)])

        # Dot product to check the angle between the normals and lane directions
        
        angles = np.einsum('ij,kj->ik', normals, lane_dir)

        if right:
            angles = angles*-1
        
        # We want to find lanes that have a similar direction to the normals
        # You can adjust this condition based on your specific requirements
        if (angles > 0).all():
            distances = cdist(buffers, polyline)
            lane_distance = np.min(distances)
            if lane_distance < min_distance:
                min_distance = lane_distance
                closest_lane = lane

    return closest_lane
import matplotlib.pyplot as plt

def plot_lanes(target_lane, left_lane, right_lane):
    # Function to plot a lane
    def plot_lane(lane, color, name =""):
        if lane is None:
            return
        if hasattr(lane,"center"):
            points = lane.center.points
        else:
            points = lane
        x = points[:, 0]
        y = points[:, 1]
        plt.plot(x, y, color=color, label = name)
    plt.figure()
    plot_lane(target_lane, 'r', name ="target")   # Plot left lane in red
    plot_lane(left_lane, 'g', name ="left") # Plot target lane in green
    plot_lane(right_lane, 'b', name ="right")  # Plot right lane in blue

    plt.xlabel('X')
    plt.ylabel('Y')
    plt.title('Lanes Visualization')
    plt.legend()
    plt.axis('equal') # To ensure that the scale is the same on the x and y axes
    if hasattr(target_lane,"center"):
        plt.savefig(f"visualization/adjacent{target_lane.id}")
    else:
        plt.savefig(f"visualization/adjacent{target_lane[0]}.png")
    plt.close()
def assert_continuous_and_bounded(nt_arr, max_n_rate = 10 *0.1 , max_t_rate = 30 *0.1):
    # Calculate rate of change for n and t
    delta_n = np.diff(nt_arr[:, 0])
    delta_t = np.diff(nt_arr[:, 1])

    #check the maximum rate between consecutive points in n and t directions
    assert np.all(delta_n <= max_n_rate), f"Discontinuity in n detected. Maximum observed rate: {nt_arr[:, 0]}"
    assert np.all(delta_t <= max_t_rate), f"Discontinuity in t detected. Maximum observed rate: {delta_t}"
def check_continuous_and_bounded(nt_arr, max_n_rate=1.0, max_t_rate=3.0):
    # Initialize flags
    n_violated = False
    t_violated = False

    # Calculate rate of change for n and t
    delta_n = np.diff(nt_arr[:, 0])
    delta_t = np.diff(nt_arr[:, 1])

    # Check the maximum rate between consecutive points in n and t directions
    if not np.all(delta_n <= max_n_rate):
        n_violated = True
        print(f"Discontinuity in n detected. Maximum observed rate: {delta_n.max()}")

    if not np.all(delta_t <= max_t_rate):
        t_violated = True
        print(f"Discontinuity in t detected. Maximum observed rate: {delta_t.max()}")

    return n_violated, t_violated

import numpy as np

def check_and_extend_centerline(test_path, chopped_center_line_xy, centerline_heading, some_distance_threshold=5, some_heading_threshold=np.pi/8):
    vehicle_point = test_path[0, :2]  # Only pick (x, y)
    vehicle_heading = test_path[0, 3]
    
    # Find the closest point on the centerline to the vehicle
    closest_idx = np.argmin(np.linalg.norm(chopped_center_line_xy - vehicle_point, axis=1))

    # Segments ahead and behind the vehicle on the centerline
    ahead_segment = chopped_center_line_xy[closest_idx:, :]
    behind_segment = chopped_center_line_xy[:closest_idx, :]

    extended_centerline = chopped_center_line_xy
    extended_heading = centerline_heading

    if len(behind_segment) < some_distance_threshold:
        # Your existing logic for when to extend the centerline
        first_centerline_point = chopped_center_line_xy[0, :2]  # Only pick (x, y)
        first_centerline_heading = centerline_heading[0]

        # Calculate Euclidean distance in XY
        distance = np.linalg.norm(vehicle_point - first_centerline_point)

        # Calculate difference in heading
        heading_diff = np.abs(vehicle_heading - first_centerline_heading)
        heading_diff = np.min([heading_diff, 2 * np.pi - heading_diff])  # Taking the smaller angle

        if distance > some_distance_threshold or heading_diff > some_heading_threshold:
            # Compute distance between first_centerline_point and vehicle_point
            distance = np.linalg.norm(first_centerline_point - vehicle_point)

            # Compute the number of points needed for 0.5m spacing
            n_points = int(np.ceil(distance / 0.5))

            # Compute the heading for the new segment
            new_heading = np.arctan2(first_centerline_point[1] - vehicle_point[1], first_centerline_point[0] - vehicle_point[0])

            # Generate normalized t_values between 0 and 1
            t_values = np.linspace(0, 1, n_points).reshape(-1, 1)

            # Compute intermediate points
            intermediate_points = vehicle_point + t_values * (first_centerline_point - vehicle_point)

            # Stack them to the existing centerline
            extended_centerline = np.vstack([intermediate_points, chopped_center_line_xy])

            # Extend the heading array
            extended_heading = np.vstack([np.full(n_points, new_heading)[:,None], centerline_heading])

    return extended_centerline, extended_heading
