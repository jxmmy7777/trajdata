from trajdata.data_structures.batch_element import AgentBatchElement

def is_stationary(element):
    return element.agent_meta_dict["is_stationary"]