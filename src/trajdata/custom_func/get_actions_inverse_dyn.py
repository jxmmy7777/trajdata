from trajdata.data_structures.batch_element import AgentBatchElement
import numpy as np
def get_actions_inversdyn(element: AgentBatchElement) -> np.ndarray:
    """
    Calculates target actions using inverse dynamics based on an agent's historical and future states.

    Args:
        element (AgentBatchElement): An object containing an agent's historical and future trajectory data.

    Returns:
        np.ndarray: Target actions derived from inverse dynamics calculations.
    """
    # Extract position, yaw, and speed from historical and future states
    pos_hist, yaw_hist, speed_hist = trajdata2posyawspeed(element.agent_history_np)
    pos_future, yaw_future, speed_future = trajdata2posyawspeed(element.agent_future_np)

    # Append the last historical state to the beginning of the future states for continuity
    pos = np.vstack([pos_hist[-1:], pos_future])
    yaw = np.vstack([yaw_hist[-1:], yaw_future])
    speed = np.vstack([speed_hist[-1:], speed_future])

    target_states = np.hstack([pos, speed, yaw])  # Shape: (T+1, 4) with T histoy+fut time length

    # Calculate target actions using inverse dynamics
    target_actions = inverse_dynamics(target_states, element.dt)  # Shape: (T, 2)


    return target_actions

def trajdata2posyawspeed(state):
    """
    Converts trajectory data to position, yaw, and speed arrays.
    
    Args:
        state (StateTensor or StateArray): The state information containing position, heading, and speed.
    
    Returns:
        tuple: Three arrays corresponding to position, yaw, and speed.
    """
    pos = state.position
    yaw = state.heading
    speed = state.as_format("v_lon")
    
    return pos, yaw, speed
def inverse_dynamics(x, dt):
    return (x[1:,2:] - x[:-1,2:]) / dt
    