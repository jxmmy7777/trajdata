from trajdata.data_structures.batch_element import AgentBatchElement

'''
trajdata has a bad lane association for waymo, so we directly read vectormap information from this funciton
'''
import pickle
from pathlib import Path
from trajdata.custom_func import common_utils
import numpy as np
import torch
# Define the mappings for each dataset split
mappings = {
    'waymo_train': {
        "0": "39a0cef08ced6bb", "1": "d4afd959f1622346", "2": "9b0115b5ab5f3b92", "3": "f5920e4840183bfe", "4": "ec665f5bee06babd", "5": "d0118520c5120e14", "6": "2e302a292bdae928", "7": "7c1404267a2b9664", "8": "f6b5ec3990134b35", "9": "2b5c52db158f771c", "10": "cbb2e7d64472f8b", "11": "85a94593c278775f", "12": "96b09568ab334eec", "13": "615b1a1c2d17fa04", "14": "2ae43cd749228eae", "15": "4c4f9387d95fc5f8", "16": "600275e979239412", "17": "9edfe21cd9e46485", "18": "de66868b6eb72128", "19": "560411da56726d35", "20": "9617d33d8ab734f5", "21": "48bcc2e9c1794921", "22": "f0ceb981456e5fe0", "23": "8c957393626cda96", "24": "540719f3a24370af", "25": "fd2b825eece04bf8", "26": "873c3afae1dfdd65", "27": "2bdc21b6461f8a65", "28": "18a02d58b7e4120", "29": "681467833f94c27d", "30": "bb1f965b3cf52213", "31": "65f4ccadcbf2c676", "32": "92bb0cbeb41820b6", "33": "2fdf768139861575", "34": "ac0fd76e895c1ae", "35": "e65c6a07c375c724", "36": "cfd7684e2b17a9be", "37": "b214da2f20c418", "38": "6acd7ee8f740f6e6", "39": "d2ae9a217f3188e0", "40": "16007b38794f18e", "41": "731e93f480f9702c", "42": "b66d9dc0309be498", "43": "761425d31e32db32", "44": "e970461587492fdd", "45": "a37a9964b0b53e0", "46": "f6c91a19262bd227", "47": "3b4605d483db05a5", "48": "5ef870ea031e33fd", "49": "e4637a37b8cfcefd", "50": "a3f6548174d5662a", "51": "5997aa2ffcbccc1b", "52": "9049340f448013de", "53": "75f52c382954e489", "54": "5da30ae24970086e", "55": "b7355f563611b128", "56": "86fc61fd80ba7dc3", "57": "ada8be4a76d7f83c", "58": "528929fa0f00297", "59": "7318349e22bab361", "60": "e6cad7dfd2e5bc2c", "61": "38d453e2bfa4d83e", "62": "e79aec7df1d9fa60", "63": "f6536858f7c786", "64": "13a2d23f490dd63d", "65": "7a1106f025b5bec8", "66": "583cf395ea0f3f78", "67": "f49ad01deda6c558", "68": "3eedfd7d6bb8213b", "69": "f1c819380374e59c", "70": "95fa94d3b3e1f3c6", "71": "597e2d60a5ddf3e0", "72": "cb3790f09caa6c76", "73": "8e7704f452d69476", "74": "90c7ab78b2f946e1", "75": "109bafe59bf75642", "76": "375d2b04ec4168a0", "77": "3e837d5953124739", "78": "9fabf08d14f4773a", "79": "4673dd742b7a2b3f", "80": "efc5f701afd88f0d", "81": "66e162734c77d787", "82": "be63ea7fc1f5af0a", "83": "e6b3a9e1f99d4248", "84": "fe266f1746487e40", "85": "d7d1e9f9e8a02d77", "86": "9ea6f6f745a1aaef", "87": "af88e5c254a7cf7d", "88": "ef5934a076e24939", "89": "faa2f74aa6dd484e", "90": "3979c03c68c0ed57", "91": "68820b041e2f7e7", "92": "b207fe66e9c7ec7c", "93": "d2d8ad31de5977b3", "94": "5f1c68bac3f24d6b", "95": "ccb0cc6c4bc93aa3", "96": "4aa3dc5a5d2d036f", "97": "9a566cd14889c4f1", "98": "cc871f66a7898bd5", "99": "260785192cf6c991", "100": "5fce75210d0c3e5b", "101": "8832e78ddcafe417", "102": "f2c7c4761864a0d4", "103": "1c8c1efe9255be88", "104": "64372d9615bcd985", "105": "8944bbe8eceffe8f", "106": "679b3c8983553084", "107": "bae9ac9a947adeaa", "108": "c7f51c9e08faa328", "109": "1f52748af0a9838b", "110": "7fab869868e4c830", "111": "a172c1ca5508b3d1", "112": "cad481733b3e17a3", "113": "5eea5080e97ee2cf", "114": "2c41e430936b16be", "115": "4708bd1d7fb9c467", "116": "b845d77d3f477abf", "117": "3b3f031b9a38325a", "118": "639c0c6962234d8a", "119": "b5815b6db1445093", "120": "d267d9754161b910", "121": "1ee3561ee771c358", "122": "91536ae5567cd256", "123": "5f4b4313d43cda1f", "124": "369efe495fe759ce", "125": "7302d8b460ccc420", "126": "4dd5ac169be5fe8", "127": "c6ca47e03dcf4b94", "128": "6530c449cc121095", "129": "aed0e291d6462fda", "130": "46a13f362dc0a5d0", "131": "9b0b6db49e480020", "132": "6fdbd98d56393021", "133": "3a6494b2a6b9ab9e", "134": "641655bb8d28a2e6", "135": "fd84b94db814709a", "136": "336b6bb1e0ddd8c7", "137": "3ca031cb4a90d24b", "138": "ca69aace5ae1bb78", "139": "827b3531de544b2c", "140": "89ff44c4b969da1e", "141": "12b7026ddd96309d", "142": "17c139daf05dc9dc", "143": "e6835926bb745df1", "144": "42e3bc41d3c0c1e2", "145": "35f4e42278fc3e4d", "146": "de95ca0b399587b5", "147": "b09fb1e524f725a6", "148": "3fb8f5f3bdbd37d4", "149": "7f0132c55a4023ae", "150": "75e425474eef56c4", "151": "f196b625cbf83b34", "152": "25fc9d891559f02b", "153": "dbc138d6ee9d07b6", "154": "dd05436fb7a4f2d1", "155": "8e3aa17989eb0f3a", "156": "101ba4c98d705f0", "157": "7a425f02908d29c9", "158": "9e67d66829169b41", "159": "2cbe5f006f84fcb1", "160": "fa3f8176d94bad8a", "161": "38f2fce8cf76f8ac", "162": "1629a765c1d0fc24", "163": "7f636cd96b17f0d8", "164": "81a20236e1d85d45", "165": "37e6ba9909c3fe29", "166": "68938f248f80da1d", "167": "6390e5a6eb757c33", "168": "4017ba2898a0219f", "169": "51f10c3af1c14eda", "170": "1a1a379c4e09cc59", "171": "4cf3d867308dd58", "172": "b212f8ab07ddceb", "173": "6d179e205e6dcb94", "174": "4492466e3d320dd5", "175": "7082fa668687ef79", "176": "a6936fc1b43f918d", "177": "11c4b7642342ed90", "178": "e95c8f05b09c2be3", "179": "54e7ee570dcd34e7", "180": "23020093b9368687", "181": "c046baaf7043adcc", "182": "f29a56d80885dcf3", "183": "4fe760ac15e16924", "184": "39ad3fc541a0e852", "185": "3d5d2d2afa2089fa", "186": "440288332f1de399", "187": "daf699e54d1363a4", "188": "3c2e4f672d8da0e5", "189": "bc76b064e334ebe4", "190": "11facf529e2c7955", "191": "1d58cf1549a75e99", "192": "14af575205c6a26f", "193": "f34f0f3757eb23ea", "194": "ca6fa1a25206ecc", "195": "a1843806f36ce4e8", "196": "c01d933f4e5f6477", "197": "d49e92f28e602fab", "198": "a1d414be68affecf", "199": "673d2526c48f9fa0", "200": "c4b68e0369a94188", "201": "abfe083025dfd22b", "202": "843b59006e5fb20b", "203": "6f827e0a6e136596", "204": "f73df86592323d53", "205": "61d1e56c486da9fb", "206": "f8c94e681fa2aeeb", "207": "586ac96409a09518", "208": "8873fe1e03e6af29", "209": "9cddb4de10e6135d", "210": "f246f13f819c7f3f", "211": "e99a6837fe63fda4", "212": "eb786ab2ed1c47fe", "213": "2db695e9619867ac", "214": "7af36acf5e6f74ab", "215": "e3fa844a429fbfd1", "216": "4b724e6537d3c59", "217": "cd2fba687f1c8b42", "218": "321d454418716e5d", "219": "e6d5c20551b2a22a", "220": "c3cf9e0502076f73", "221": "a68f164f343cbb47", "222": "cc3753f22704c86b", "223": "481db0d975883d94", "224": "afbdd44ddebeefad", "225": "e9d7b3a24704e908", "226": "34d3bd03501098dd", "227": "1c3773b05dc14eb9", "228": "796d74f97e7469a1", "229": "669be35f85f4ee6b", "230": "594fbe1513e922c", "231": "76c977f0860e5d25", "232": "42f251317ab551fd", "233": "2bea50b49da6f2fa", "234": "d9fce37b7de75e04", "235": "5a2a92cca020cc61", "236": "d6e25a0572abc0b1", "237": "7b22c2b05d501ee1", "238": "e5d0a6b0f649c864", "239": "63284f91dce67336", "240": "cd02036182bd309", "241": "2f1be38b374fe821", "242": "636e26428cb9fdc", "243": "e8ad488730df425a", "244": "d988a95fdd1faa5", "245": "e72a2c0bb20805c8", "246": "d39e17152d71dfd9", "247": "e1aeba17560793cc", "248": "b62f56c744931e2c", "249": "9c8424e99f143a73", "250": "abea7f93eedb3250", "251": "29ea9d7adb183086", "252": "b2cf06ad6c706afe", "253": "e6fade751927b347", "254": "84d2d908b14dd438", "255": "32504316ee1f1c8f", "256": "63428d819abbe654", "257": "bc9d2eaf6bec84f8", "258": "b2424ed7a3d9ae9b", "259": "ace8b998e535bd94", "260": "8c1d2c37e868c5b1", "261": "a7e2eaa8635c1cf2", "262": "c90a65626a243d6", "263": "ffcd7672abe0a305", "264": "7dc03bd771f54e72", "265": "f0c661ebb4f650db", "266": "f1f8620785ad064c", "267": "91fefc517ee432ce", "268": "c977fd881dd797b", "269": "a5704e8a58571f00", "270": "8aedb13797e8d9d2", "271": "89715b7a60baef38", "272": "d44c0b098310bd91", "273": "d4b9cd18b3c64c57", "274": "f6e36094a3f98de2", "275": "9caa66e19818ad62", "276": "aa65d1fb18deb756", "277": "e077263b9fdab6aa", "278": "fa5c2f92c60076a7", "279": "3febcf861d780274", "280": "e24bd3e8ab644ae0", "281": "8a157cf38178017e", "282": "9000b6225cd6a48a", "283": "1fcac8c108670058", "284": "80fb02f490a50cff", "285": "541d6dbf9aa1f3ee", "286": "6ef8d0c911db2408", "287": "9919840473935527", "288": "5a4e6ea0937ecf82", "289": "61ea8e1e5f7f88a5", "290": "f84ee5e29839d6d2", "291": "ff4981b2757eb960", "292": "2e50eb3736ce4737", "293": "baa3b9eaacd0026b", "294": "35f12b603a20c461", "295": "cf1454d67373ac9f", "296": "bc4960f478da2749", "297": "b1c609e8523c3ae4", "298": "f6451bf71678d897", "299": "82e3ab4129efddb6", "300": "c3ad7a790aae7503", "301": "8062c896c4fadc7e"
    },
    'waymo_val': {
       "0": "39a0cef08ced6bb", "1": "d4afd959f1622346", "2": "9b0115b5ab5f3b92", "3": "f5920e4840183bfe", "4": "ec665f5bee06babd", "5": "d0118520c5120e14", "6": "2e302a292bdae928", "7": "7c1404267a2b9664", "8": "f6b5ec3990134b35", "9": "2b5c52db158f771c", "10": "cbb2e7d64472f8b", "11": "85a94593c278775f", "12": "96b09568ab334eec", "13": "615b1a1c2d17fa04", "14": "2ae43cd749228eae", "15": "4c4f9387d95fc5f8", "16": "600275e979239412", "17": "9edfe21cd9e46485", "18": "de66868b6eb72128", "19": "560411da56726d35", "20": "9617d33d8ab734f5", "21": "48bcc2e9c1794921", "22": "f0ceb981456e5fe0", "23": "8c957393626cda96", "24": "540719f3a24370af", "25": "fd2b825eece04bf8", "26": "873c3afae1dfdd65", "27": "2bdc21b6461f8a65", "28": "18a02d58b7e4120", "29": "681467833f94c27d", "30": "bb1f965b3cf52213", "31": "65f4ccadcbf2c676", "32": "92bb0cbeb41820b6", "33": "2fdf768139861575", "34": "ac0fd76e895c1ae", "35": "e65c6a07c375c724", "36": "cfd7684e2b17a9be", "37": "b214da2f20c418", "38": "6acd7ee8f740f6e6", "39": "d2ae9a217f3188e0", "40": "16007b38794f18e", "41": "731e93f480f9702c", "42": "b66d9dc0309be498", "43": "761425d31e32db32", "44": "e970461587492fdd", "45": "a37a9964b0b53e0", "46": "f6c91a19262bd227", "47": "3b4605d483db05a5", "48": "5ef870ea031e33fd", "49": "e4637a37b8cfcefd", "50": "a3f6548174d5662a", "51": "5997aa2ffcbccc1b", "52": "9049340f448013de", "53": "75f52c382954e489", "54": "5da30ae24970086e", "55": "b7355f563611b128", "56": "86fc61fd80ba7dc3", "57": "ada8be4a76d7f83c", "58": "528929fa0f00297", "59": "7318349e22bab361", "60": "e6cad7dfd2e5bc2c", "61": "38d453e2bfa4d83e", "62": "e79aec7df1d9fa60", "63": "f6536858f7c786", "64": "13a2d23f490dd63d", "65": "7a1106f025b5bec8", "66": "583cf395ea0f3f78", "67": "f49ad01deda6c558", "68": "3eedfd7d6bb8213b", "69": "f1c819380374e59c", "70": "95fa94d3b3e1f3c6", "71": "597e2d60a5ddf3e0", "72": "cb3790f09caa6c76", "73": "8e7704f452d69476", "74": "90c7ab78b2f946e1", "75": "109bafe59bf75642", "76": "375d2b04ec4168a0", "77": "3e837d5953124739", "78": "9fabf08d14f4773a", "79": "4673dd742b7a2b3f", "80": "efc5f701afd88f0d", "81": "66e162734c77d787", "82": "be63ea7fc1f5af0a", "83": "e6b3a9e1f99d4248", "84": "fe266f1746487e40", "85": "d7d1e9f9e8a02d77", "86": "9ea6f6f745a1aaef", "87": "af88e5c254a7cf7d", "88": "ef5934a076e24939", "89": "faa2f74aa6dd484e", "90": "3979c03c68c0ed57", "91": "68820b041e2f7e7", "92": "b207fe66e9c7ec7c", "93": "d2d8ad31de5977b3", "94": "5f1c68bac3f24d6b", "95": "ccb0cc6c4bc93aa3", "96": "4aa3dc5a5d2d036f", "97": "9a566cd14889c4f1", "98": "cc871f66a7898bd5", "99": "260785192cf6c991", "100": "5fce75210d0c3e5b", "101": "8832e78ddcafe417", "102": "f2c7c4761864a0d4", "103": "1c8c1efe9255be88", "104": "64372d9615bcd985", "105": "8944bbe8eceffe8f", "106": "679b3c8983553084", "107": "bae9ac9a947adeaa", "108": "c7f51c9e08faa328", "109": "1f52748af0a9838b", "110": "7fab869868e4c830", "111": "a172c1ca5508b3d1", "112": "cad481733b3e17a3", "113": "5eea5080e97ee2cf", "114": "2c41e430936b16be", "115": "4708bd1d7fb9c467", "116": "b845d77d3f477abf", "117": "3b3f031b9a38325a", "118": "639c0c6962234d8a", "119": "b5815b6db1445093", "120": "d267d9754161b910", "121": "1ee3561ee771c358", "122": "91536ae5567cd256", "123": "5f4b4313d43cda1f", "124": "369efe495fe759ce", "125": "7302d8b460ccc420", "126": "4dd5ac169be5fe8", "127": "c6ca47e03dcf4b94", "128": "6530c449cc121095", "129": "aed0e291d6462fda", "130": "46a13f362dc0a5d0", "131": "9b0b6db49e480020", "132": "6fdbd98d56393021", "133": "3a6494b2a6b9ab9e", "134": "641655bb8d28a2e6", "135": "fd84b94db814709a", "136": "336b6bb1e0ddd8c7", "137": "3ca031cb4a90d24b", "138": "ca69aace5ae1bb78", "139": "827b3531de544b2c", "140": "89ff44c4b969da1e", "141": "12b7026ddd96309d", "142": "17c139daf05dc9dc", "143": "e6835926bb745df1", "144": "42e3bc41d3c0c1e2", "145": "35f4e42278fc3e4d", "146": "de95ca0b399587b5", "147": "b09fb1e524f725a6", "148": "3fb8f5f3bdbd37d4", "149": "7f0132c55a4023ae", "150": "75e425474eef56c4", "151": "f196b625cbf83b34", "152": "25fc9d891559f02b", "153": "dbc138d6ee9d07b6", "154": "dd05436fb7a4f2d1", "155": "8e3aa17989eb0f3a", "156": "101ba4c98d705f0", "157": "7a425f02908d29c9", "158": "9e67d66829169b41", "159": "2cbe5f006f84fcb1", "160": "fa3f8176d94bad8a", "161": "38f2fce8cf76f8ac", "162": "1629a765c1d0fc24", "163": "7f636cd96b17f0d8", "164": "81a20236e1d85d45", "165": "37e6ba9909c3fe29", "166": "68938f248f80da1d", "167": "6390e5a6eb757c33", "168": "4017ba2898a0219f", "169": "51f10c3af1c14eda", "170": "1a1a379c4e09cc59", "171": "4cf3d867308dd58", "172": "b212f8ab07ddceb", "173": "6d179e205e6dcb94", "174": "4492466e3d320dd5", "175": "7082fa668687ef79", "176": "a6936fc1b43f918d", "177": "11c4b7642342ed90", "178": "e95c8f05b09c2be3", "179": "54e7ee570dcd34e7", "180": "23020093b9368687", "181": "c046baaf7043adcc", "182": "f29a56d80885dcf3", "183": "4fe760ac15e16924", "184": "39ad3fc541a0e852", "185": "3d5d2d2afa2089fa", "186": "440288332f1de399", "187": "daf699e54d1363a4", "188": "3c2e4f672d8da0e5", "189": "bc76b064e334ebe4", "190": "11facf529e2c7955", "191": "1d58cf1549a75e99", "192": "14af575205c6a26f", "193": "f34f0f3757eb23ea", "194": "ca6fa1a25206ecc", "195": "a1843806f36ce4e8", "196": "c01d933f4e5f6477", "197": "d49e92f28e602fab", "198": "a1d414be68affecf", "199": "673d2526c48f9fa0", "200": "c4b68e0369a94188", "201": "abfe083025dfd22b", "202": "843b59006e5fb20b", "203": "6f827e0a6e136596", "204": "f73df86592323d53", "205": "61d1e56c486da9fb", "206": "f8c94e681fa2aeeb", "207": "586ac96409a09518", "208": "8873fe1e03e6af29", "209": "9cddb4de10e6135d", "210": "f246f13f819c7f3f", "211": "e99a6837fe63fda4", "212": "eb786ab2ed1c47fe", "213": "2db695e9619867ac", "214": "7af36acf5e6f74ab", "215": "e3fa844a429fbfd1", "216": "4b724e6537d3c59", "217": "cd2fba687f1c8b42", "218": "321d454418716e5d", "219": "e6d5c20551b2a22a", "220": "c3cf9e0502076f73", "221": "a68f164f343cbb47", "222": "cc3753f22704c86b", "223": "481db0d975883d94", "224": "afbdd44ddebeefad", "225": "e9d7b3a24704e908", "226": "34d3bd03501098dd", "227": "1c3773b05dc14eb9", "228": "796d74f97e7469a1", "229": "669be35f85f4ee6b", "230": "594fbe1513e922c", "231": "76c977f0860e5d25", "232": "42f251317ab551fd", "233": "2bea50b49da6f2fa", "234": "d9fce37b7de75e04", "235": "5a2a92cca020cc61", "236": "d6e25a0572abc0b1", "237": "7b22c2b05d501ee1", "238": "e5d0a6b0f649c864", "239": "63284f91dce67336", "240": "cd02036182bd309", "241": "2f1be38b374fe821", "242": "636e26428cb9fdc", "243": "e8ad488730df425a", "244": "d988a95fdd1faa5", "245": "e72a2c0bb20805c8", "246": "d39e17152d71dfd9", "247": "e1aeba17560793cc", "248": "b62f56c744931e2c", "249": "9c8424e99f143a73", "250": "abea7f93eedb3250", "251": "29ea9d7adb183086", "252": "b2cf06ad6c706afe", "253": "e6fade751927b347", "254": "84d2d908b14dd438", "255": "32504316ee1f1c8f", "256": "63428d819abbe654", "257": "bc9d2eaf6bec84f8", "258": "b2424ed7a3d9ae9b", "259": "ace8b998e535bd94", "260": "8c1d2c37e868c5b1", "261": "a7e2eaa8635c1cf2", "262": "c90a65626a243d6", "263": "ffcd7672abe0a305", "264": "7dc03bd771f54e72", "265": "f0c661ebb4f650db", "266": "f1f8620785ad064c", "267": "91fefc517ee432ce", "268": "c977fd881dd797b", "269": "a5704e8a58571f00", "270": "8aedb13797e8d9d2", "271": "89715b7a60baef38", "272": "d44c0b098310bd91", "273": "d4b9cd18b3c64c57", "274": "f6e36094a3f98de2", "275": "9caa66e19818ad62", "276": "aa65d1fb18deb756", "277": "e077263b9fdab6aa", "278": "fa5c2f92c60076a7", "279": "3febcf861d780274", "280": "e24bd3e8ab644ae0", "281": "8a157cf38178017e", "282": "9000b6225cd6a48a", "283": "1fcac8c108670058", "284": "80fb02f490a50cff", "285": "541d6dbf9aa1f3ee", "286": "6ef8d0c911db2408", "287": "9919840473935527", "288": "5a4e6ea0937ecf82", "289": "61ea8e1e5f7f88a5", "290": "f84ee5e29839d6d2", "291": "ff4981b2757eb960", "292": "2e50eb3736ce4737", "293": "baa3b9eaacd0026b", "294": "35f12b603a20c461", "295": "cf1454d67373ac9f", "296": "bc4960f478da2749", "297": "b1c609e8523c3ae4", "298": "f6451bf71678d897", "299": "82e3ab4129efddb6", "300": "c3ad7a790aae7503", "301": "8062c896c4fadc7e"
    },
    'waymo_test': {
       "0": "39a0cef08ced6bb", "1": "d4afd959f1622346", "2": "9b0115b5ab5f3b92", "3": "f5920e4840183bfe", "4": "ec665f5bee06babd", "5": "d0118520c5120e14", "6": "2e302a292bdae928", "7": "7c1404267a2b9664", "8": "f6b5ec3990134b35", "9": "2b5c52db158f771c", "10": "cbb2e7d64472f8b", "11": "85a94593c278775f", "12": "96b09568ab334eec", "13": "615b1a1c2d17fa04", "14": "2ae43cd749228eae", "15": "4c4f9387d95fc5f8", "16": "600275e979239412", "17": "9edfe21cd9e46485", "18": "de66868b6eb72128", "19": "560411da56726d35", "20": "9617d33d8ab734f5", "21": "48bcc2e9c1794921", "22": "f0ceb981456e5fe0", "23": "8c957393626cda96", "24": "540719f3a24370af", "25": "fd2b825eece04bf8", "26": "873c3afae1dfdd65", "27": "2bdc21b6461f8a65", "28": "18a02d58b7e4120", "29": "681467833f94c27d", "30": "bb1f965b3cf52213", "31": "65f4ccadcbf2c676", "32": "92bb0cbeb41820b6", "33": "2fdf768139861575", "34": "ac0fd76e895c1ae", "35": "e65c6a07c375c724", "36": "cfd7684e2b17a9be", "37": "b214da2f20c418", "38": "6acd7ee8f740f6e6", "39": "d2ae9a217f3188e0", "40": "16007b38794f18e", "41": "731e93f480f9702c", "42": "b66d9dc0309be498", "43": "761425d31e32db32", "44": "e970461587492fdd", "45": "a37a9964b0b53e0", "46": "f6c91a19262bd227", "47": "3b4605d483db05a5", "48": "5ef870ea031e33fd", "49": "e4637a37b8cfcefd", "50": "a3f6548174d5662a", "51": "5997aa2ffcbccc1b", "52": "9049340f448013de", "53": "75f52c382954e489", "54": "5da30ae24970086e", "55": "b7355f563611b128", "56": "86fc61fd80ba7dc3", "57": "ada8be4a76d7f83c", "58": "528929fa0f00297", "59": "7318349e22bab361", "60": "e6cad7dfd2e5bc2c", "61": "38d453e2bfa4d83e", "62": "e79aec7df1d9fa60", "63": "f6536858f7c786", "64": "13a2d23f490dd63d", "65": "7a1106f025b5bec8", "66": "583cf395ea0f3f78", "67": "f49ad01deda6c558", "68": "3eedfd7d6bb8213b", "69": "f1c819380374e59c", "70": "95fa94d3b3e1f3c6", "71": "597e2d60a5ddf3e0", "72": "cb3790f09caa6c76", "73": "8e7704f452d69476", "74": "90c7ab78b2f946e1", "75": "109bafe59bf75642", "76": "375d2b04ec4168a0", "77": "3e837d5953124739", "78": "9fabf08d14f4773a", "79": "4673dd742b7a2b3f", "80": "efc5f701afd88f0d", "81": "66e162734c77d787", "82": "be63ea7fc1f5af0a", "83": "e6b3a9e1f99d4248", "84": "fe266f1746487e40", "85": "d7d1e9f9e8a02d77", "86": "9ea6f6f745a1aaef", "87": "af88e5c254a7cf7d", "88": "ef5934a076e24939", "89": "faa2f74aa6dd484e", "90": "3979c03c68c0ed57", "91": "68820b041e2f7e7", "92": "b207fe66e9c7ec7c", "93": "d2d8ad31de5977b3", "94": "5f1c68bac3f24d6b", "95": "ccb0cc6c4bc93aa3", "96": "4aa3dc5a5d2d036f", "97": "9a566cd14889c4f1", "98": "cc871f66a7898bd5", "99": "260785192cf6c991", "100": "5fce75210d0c3e5b", "101": "8832e78ddcafe417", "102": "f2c7c4761864a0d4", "103": "1c8c1efe9255be88", "104": "64372d9615bcd985", "105": "8944bbe8eceffe8f", "106": "679b3c8983553084", "107": "bae9ac9a947adeaa", "108": "c7f51c9e08faa328", "109": "1f52748af0a9838b", "110": "7fab869868e4c830", "111": "a172c1ca5508b3d1", "112": "cad481733b3e17a3", "113": "5eea5080e97ee2cf", "114": "2c41e430936b16be", "115": "4708bd1d7fb9c467", "116": "b845d77d3f477abf", "117": "3b3f031b9a38325a", "118": "639c0c6962234d8a", "119": "b5815b6db1445093", "120": "d267d9754161b910", "121": "1ee3561ee771c358", "122": "91536ae5567cd256", "123": "5f4b4313d43cda1f", "124": "369efe495fe759ce", "125": "7302d8b460ccc420", "126": "4dd5ac169be5fe8", "127": "c6ca47e03dcf4b94", "128": "6530c449cc121095", "129": "aed0e291d6462fda", "130": "46a13f362dc0a5d0", "131": "9b0b6db49e480020", "132": "6fdbd98d56393021", "133": "3a6494b2a6b9ab9e", "134": "641655bb8d28a2e6", "135": "fd84b94db814709a", "136": "336b6bb1e0ddd8c7", "137": "3ca031cb4a90d24b", "138": "ca69aace5ae1bb78", "139": "827b3531de544b2c", "140": "89ff44c4b969da1e", "141": "12b7026ddd96309d", "142": "17c139daf05dc9dc", "143": "e6835926bb745df1", "144": "42e3bc41d3c0c1e2", "145": "35f4e42278fc3e4d", "146": "de95ca0b399587b5", "147": "b09fb1e524f725a6", "148": "3fb8f5f3bdbd37d4", "149": "7f0132c55a4023ae", "150": "75e425474eef56c4", "151": "f196b625cbf83b34", "152": "25fc9d891559f02b", "153": "dbc138d6ee9d07b6", "154": "dd05436fb7a4f2d1", "155": "8e3aa17989eb0f3a", "156": "101ba4c98d705f0", "157": "7a425f02908d29c9", "158": "9e67d66829169b41", "159": "2cbe5f006f84fcb1", "160": "fa3f8176d94bad8a", "161": "38f2fce8cf76f8ac", "162": "1629a765c1d0fc24", "163": "7f636cd96b17f0d8", "164": "81a20236e1d85d45", "165": "37e6ba9909c3fe29", "166": "68938f248f80da1d", "167": "6390e5a6eb757c33", "168": "4017ba2898a0219f", "169": "51f10c3af1c14eda", "170": "1a1a379c4e09cc59", "171": "4cf3d867308dd58", "172": "b212f8ab07ddceb", "173": "6d179e205e6dcb94", "174": "4492466e3d320dd5", "175": "7082fa668687ef79", "176": "a6936fc1b43f918d", "177": "11c4b7642342ed90", "178": "e95c8f05b09c2be3", "179": "54e7ee570dcd34e7", "180": "23020093b9368687", "181": "c046baaf7043adcc", "182": "f29a56d80885dcf3", "183": "4fe760ac15e16924", "184": "39ad3fc541a0e852", "185": "3d5d2d2afa2089fa", "186": "440288332f1de399", "187": "daf699e54d1363a4", "188": "3c2e4f672d8da0e5", "189": "bc76b064e334ebe4", "190": "11facf529e2c7955", "191": "1d58cf1549a75e99", "192": "14af575205c6a26f", "193": "f34f0f3757eb23ea", "194": "ca6fa1a25206ecc", "195": "a1843806f36ce4e8", "196": "c01d933f4e5f6477", "197": "d49e92f28e602fab", "198": "a1d414be68affecf", "199": "673d2526c48f9fa0", "200": "c4b68e0369a94188", "201": "abfe083025dfd22b", "202": "843b59006e5fb20b", "203": "6f827e0a6e136596", "204": "f73df86592323d53", "205": "61d1e56c486da9fb", "206": "f8c94e681fa2aeeb", "207": "586ac96409a09518", "208": "8873fe1e03e6af29", "209": "9cddb4de10e6135d", "210": "f246f13f819c7f3f", "211": "e99a6837fe63fda4", "212": "eb786ab2ed1c47fe", "213": "2db695e9619867ac", "214": "7af36acf5e6f74ab", "215": "e3fa844a429fbfd1", "216": "4b724e6537d3c59", "217": "cd2fba687f1c8b42", "218": "321d454418716e5d", "219": "e6d5c20551b2a22a", "220": "c3cf9e0502076f73", "221": "a68f164f343cbb47", "222": "cc3753f22704c86b", "223": "481db0d975883d94", "224": "afbdd44ddebeefad", "225": "e9d7b3a24704e908", "226": "34d3bd03501098dd", "227": "1c3773b05dc14eb9", "228": "796d74f97e7469a1", "229": "669be35f85f4ee6b", "230": "594fbe1513e922c", "231": "76c977f0860e5d25", "232": "42f251317ab551fd", "233": "2bea50b49da6f2fa", "234": "d9fce37b7de75e04", "235": "5a2a92cca020cc61", "236": "d6e25a0572abc0b1", "237": "7b22c2b05d501ee1", "238": "e5d0a6b0f649c864", "239": "63284f91dce67336", "240": "cd02036182bd309", "241": "2f1be38b374fe821", "242": "636e26428cb9fdc", "243": "e8ad488730df425a", "244": "d988a95fdd1faa5", "245": "e72a2c0bb20805c8", "246": "d39e17152d71dfd9", "247": "e1aeba17560793cc", "248": "b62f56c744931e2c", "249": "9c8424e99f143a73", "250": "abea7f93eedb3250", "251": "29ea9d7adb183086", "252": "b2cf06ad6c706afe", "253": "e6fade751927b347", "254": "84d2d908b14dd438", "255": "32504316ee1f1c8f", "256": "63428d819abbe654", "257": "bc9d2eaf6bec84f8", "258": "b2424ed7a3d9ae9b", "259": "ace8b998e535bd94", "260": "8c1d2c37e868c5b1", "261": "a7e2eaa8635c1cf2", "262": "c90a65626a243d6", "263": "ffcd7672abe0a305", "264": "7dc03bd771f54e72", "265": "f0c661ebb4f650db", "266": "f1f8620785ad064c", "267": "91fefc517ee432ce", "268": "c977fd881dd797b", "269": "a5704e8a58571f00", "270": "8aedb13797e8d9d2", "271": "89715b7a60baef38", "272": "d44c0b098310bd91", "273": "d4b9cd18b3c64c57", "274": "f6e36094a3f98de2", "275": "9caa66e19818ad62", "276": "aa65d1fb18deb756", "277": "e077263b9fdab6aa", "278": "fa5c2f92c60076a7", "279": "3febcf861d780274", "280": "e24bd3e8ab644ae0", "281": "8a157cf38178017e", "282": "9000b6225cd6a48a", "283": "1fcac8c108670058", "284": "80fb02f490a50cff", "285": "541d6dbf9aa1f3ee", "286": "6ef8d0c911db2408", "287": "9919840473935527", "288": "5a4e6ea0937ecf82", "289": "61ea8e1e5f7f88a5", "290": "f84ee5e29839d6d2", "291": "ff4981b2757eb960", "292": "2e50eb3736ce4737", "293": "baa3b9eaacd0026b", "294": "35f12b603a20c461", "295": "cf1454d67373ac9f", "296": "bc4960f478da2749", "297": "b1c609e8523c3ae4", "298": "f6451bf71678d897", "299": "82e3ab4129efddb6", "300": "c3ad7a790aae7503", "301": "8062c896c4fadc7e"
    }
}
tag2name = {
    "waymo_train":"training",
    "waymo_val":"validation",
    "waymo_test":"testing"
}

ROOT_DIR = Path("/mnt/hdd2/weijer/MTR")

# need to ensure the size are the same?
dataset_cfg =   {
    "POINT_SAMPLED_INTERVAL": 1,
    "NUM_POINTS_EACH_POLYLINE": 20,
    "VECTOR_BREAK_DIST_THRESH": 1.0,

    "NUM_OF_SRC_POLYLINES": 768,
    "CENTER_OFFSET_OF_MAP": (30.0, 0),
}
def get_waymo_map(element):
    scenario_idx = (element.scene_id.split("_")[1])  # Assuming scene_id is like 'scene_idx'
    tag = element.map_name.split(":")[0]  # Assuming map_name is like 'tag:something'
    
    # Select the correct scenario ID map based on the tag
    if tag in mappings:
        scenario_id = mappings[tag].get(scenario_idx)
        if scenario_id is None:
            raise ValueError(f"No scenario_id found for index {scenario_idx} in {tag} data")
    else:
        raise ValueError(f"Unsupported tag {tag}")

    # Read from the appropriate file
    file_path = ROOT_DIR / f"processed_scenarios_{tag2name[tag]}" / f"sample_{scenario_id}.pkl"
    with open(file_path, 'rb') as f:
        info = pickle.load(f)
    
    #TODO choose topk polylines?
    ret_dict = {}
    #center objects is (1, 10): [cx, cy, cz, dx, dy, dz, heading, vel_x, vel_y, valid] 
    agent_extents = element.agent_history_extents[0][0]
    center_agent_state = element.centered_agent_state_np
    center_objects = np.hstack(
        (center_agent_state[:3],
         agent_extents,
         center_agent_state[-1:],
         center_agent_state[3:5],
         np.array(1), #should be valid
        )
    )[None]
    map_polylines_data, map_polylines_mask, map_polylines_center = create_map_data_for_center_objects(
        center_objects=center_objects, map_infos=info['map_infos'],
        center_offset=dataset_cfg.get('CENTER_OFFSET_OF_MAP', (30.0, 0)),
    )   # (num_center_objects, num_topk_polylines, num_points_each_polyline, 9), (num_center_objects, num_topk_polylines, num_points_each_polyline)

    ret_dict['map_polylines'] = map_polylines_data
    ret_dict['map_polylines_mask'] = (map_polylines_mask > 0)
    ret_dict['map_polylines_center'] = map_polylines_center
    
    
    return ret_dict


# Adapted from MTR
@staticmethod
def generate_batch_polylines_from_map(polylines, point_sampled_interval=1, vector_break_dist_thresh=1.0, num_points_each_polyline=20):
    """
    Args:
        polylines (num_points, 7): [x, y, z, dir_x, dir_y, dir_z, global_type]

    Returns:
        ret_polylines: (num_polylines, num_points_each_polyline, 7)
        ret_polylines_mask: (num_polylines, num_points_each_polyline)
    """
    point_dim = polylines.shape[-1]

    sampled_points = polylines[::point_sampled_interval]
    sampled_points_shift = np.roll(sampled_points, shift=1, axis=0)
    buffer_points = np.concatenate((sampled_points[:, 0:2], sampled_points_shift[:, 0:2]), axis=-1) # [ed_x, ed_y, st_x, st_y]
    buffer_points[0, 2:4] = buffer_points[0, 0:2]

    break_idxs = (np.linalg.norm(buffer_points[:, 0:2] - buffer_points[:, 2:4], axis=-1) > vector_break_dist_thresh).nonzero()[0]
    polyline_list = np.array_split(sampled_points, break_idxs, axis=0)
    ret_polylines = []
    ret_polylines_mask = []

    def append_single_polyline(new_polyline):
        cur_polyline = np.zeros((num_points_each_polyline, point_dim), dtype=np.float32)
        cur_valid_mask = np.zeros((num_points_each_polyline), dtype=np.int32)
        cur_polyline[:len(new_polyline)] = new_polyline
        cur_valid_mask[:len(new_polyline)] = 1
        ret_polylines.append(cur_polyline)
        ret_polylines_mask.append(cur_valid_mask)

    for k in range(len(polyline_list)):
        if polyline_list[k].__len__() <= 0:
            continue
        for idx in range(0, len(polyline_list[k]), num_points_each_polyline):
            append_single_polyline(polyline_list[k][idx: idx + num_points_each_polyline])

    ret_polylines = np.stack(ret_polylines, axis=0)
    ret_polylines_mask = np.stack(ret_polylines_mask, axis=0)

    ret_polylines = torch.from_numpy(ret_polylines)
    ret_polylines_mask = torch.from_numpy(ret_polylines_mask)

    # # CHECK the results
    # polyline_center = ret_polylines[:, :, 0:2].sum(dim=1) / ret_polyline_valid_mask.sum(dim=1).float()[:, None]  # (num_polylines, 2)
    # center_dist = (polyline_center - ret_polylines[:, 0, 0:2]).norm(dim=-1)
    # assert center_dist.max() < 10
    return ret_polylines, ret_polylines_mask

def create_map_data_for_center_objects(center_objects, map_infos, center_offset):
    """
    Args:
        center_objects (num_center_objects, 10): [cx, cy, cz, dx, dy, dz, heading, vel_x, vel_y, valid]
        map_infos (dict):
            all_polylines (num_points, 7): [x, y, z, dir_x, dir_y, dir_z, global_type]
        center_offset (2):, [offset_x, offset_y]
    Returns:
        map_polylines (num_center_objects, num_topk_polylines, num_points_each_polyline, 9): [x, y, z, dir_x, dir_y, dir_z, global_type, pre_x, pre_y]
        map_polylines_mask (num_center_objects, num_topk_polylines, num_points_each_polyline)
    """
    num_center_objects = center_objects.shape[0]

    # transform object coordinates by center objects
    def transform_to_center_coordinates(neighboring_polylines, neighboring_polyline_valid_mask):
        neighboring_polylines[:, :, :, 0:3] -= center_objects[:, None, None, 0:3]
        neighboring_polylines[:, :, :, 0:2] = common_utils.rotate_points_along_z(
            points=neighboring_polylines[:, :, :, 0:2].view(num_center_objects, -1, 2),
            angle=-center_objects[:, 6]
        ).view(num_center_objects, -1, batch_polylines.shape[1], 2)
        neighboring_polylines[:, :, :, 3:5] = common_utils.rotate_points_along_z(
            points=neighboring_polylines[:, :, :, 3:5].view(num_center_objects, -1, 2),
            angle=-center_objects[:, 6]
        ).view(num_center_objects, -1, batch_polylines.shape[1], 2)

        # use pre points to map
        # (num_center_objects, num_polylines, num_points_each_polyline, num_feat)
        xy_pos_pre = neighboring_polylines[:, :, :, 0:2]
        xy_pos_pre = torch.roll(xy_pos_pre, shifts=1, dims=-2)
        xy_pos_pre[:, :, 0, :] = xy_pos_pre[:, :, 1, :]
        neighboring_polylines = torch.cat((neighboring_polylines, xy_pos_pre), dim=-1)

        neighboring_polylines[neighboring_polyline_valid_mask == 0] = 0
        return neighboring_polylines, neighboring_polyline_valid_mask

    polylines = torch.from_numpy(map_infos['all_polylines'].copy())
    center_objects = torch.from_numpy(center_objects)

    batch_polylines, batch_polylines_mask = generate_batch_polylines_from_map(
        polylines=polylines.numpy(), point_sampled_interval=dataset_cfg.get('POINT_SAMPLED_INTERVAL', 1),
        vector_break_dist_thresh=dataset_cfg.get('VECTOR_BREAK_DIST_THRESH', 1.0),
        num_points_each_polyline=dataset_cfg.get('NUM_POINTS_EACH_POLYLINE', 20),
    )  # (num_polylines, num_points_each_polyline, 7), (num_polylines, num_points_each_polyline)

    # collect a number of closest polylines for each center objects
    num_of_src_polylines = dataset_cfg.get("NUM_OF_SRC_POLYLINES", 768)

    if len(batch_polylines) > num_of_src_polylines:
        polyline_center = batch_polylines[:, :, 0:2].sum(dim=1) / torch.clamp_min(batch_polylines_mask.sum(dim=1).float()[:, None], min=1.0)
        center_offset_rot = torch.from_numpy(np.array(center_offset, dtype=np.float32))[None, :].repeat(num_center_objects, 1)
        center_offset_rot = common_utils.rotate_points_along_z(
            points=center_offset_rot.view(num_center_objects, 1, 2),
            angle=center_objects[:, 6]
        ).view(num_center_objects, 2)

        pos_of_map_centers = center_objects[:, 0:2] + center_offset_rot

        dist = (pos_of_map_centers[:, None, :] - polyline_center[None, :, :]).norm(dim=-1)  # (num_center_objects, num_polylines)
        topk_dist, topk_idxs = dist.topk(k=num_of_src_polylines, dim=-1, largest=False)
        map_polylines = batch_polylines[topk_idxs]  # (num_center_objects, num_topk_polylines, num_points_each_polyline, 7)
        map_polylines_mask = batch_polylines_mask[topk_idxs]  # (num_center_objects, num_topk_polylines, num_points_each_polyline)
    else:
        map_polylines = batch_polylines[None, :, :, :].repeat(num_center_objects, 1, 1, 1)
        map_polylines_mask = batch_polylines_mask[None, :, :].repeat(num_center_objects, 1, 1)

    map_polylines, map_polylines_mask = transform_to_center_coordinates(
        neighboring_polylines=map_polylines,
        neighboring_polyline_valid_mask=map_polylines_mask
    )

    temp_sum = (map_polylines[:, :, :, 0:3] * map_polylines_mask[:, :, :, None].float()).sum(dim=-2)  # (num_center_objects, num_polylines, 3)
    map_polylines_center = temp_sum / torch.clamp_min(map_polylines_mask.sum(dim=-1).float()[:, :, None], min=1.0)  # (num_center_objects, num_polylines, 3)

    map_polylines = map_polylines.numpy()
    map_polylines_mask = map_polylines_mask.numpy()
    map_polylines_center = map_polylines_center.numpy()

    return map_polylines, map_polylines_mask, map_polylines_center